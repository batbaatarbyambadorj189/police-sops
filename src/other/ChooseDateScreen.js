import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  useWindowDimensions,
  Pressable,
  TouchableOpacity,
} from "react-native";
import { LocaleConfig, Calendar } from "react-native-calendars";
import { StackActions, useNavigation } from "@react-navigation/native";
import { BlurView } from "expo-blur";
import { AntDesign } from "@expo/vector-icons";
import moment from "moment";
import dateformat from "dateformat";
import config from "../config";
LocaleConfig.locales["mn"] = config.mn;
LocaleConfig.defaultLocale = "mn";
const ChooseDateScreen = ({
  route: {
    params: { setDate, date },
  },
  navigation,
}) => {
  const dimesions = useWindowDimensions();
  const navigations = useNavigation();
  const {dispatch} = useNavigation();
  //console.log("date1", date);
  //console.log("date2", Date.parse(date));
  //console.log("date3", new Date());
  const [choosedDate, setChoosedDate] = useState(
    date ? dateformat(date, "yyyy-mm-dd") : dateformat(new Date(), "yyyy-mm-dd")
  );

  const close = () => {
    setDate(choosedDate);
    navigations.goBack();
  };

  return (
    <BlurView
      intensity={10}
      style={{
        flex: 1,
        justifyContent: "center",
        // alignSelf: "center",
      }}
    >
      <View
        style={[
          styles.modalView,
          ,
          {
            backgroundColor: "#FFF",
          },
        ]}
      >
        <Calendar
          monthFormat={`yyyy-MM`}
          renderArrow={(direction) =>
            direction == "left" ? (
              <View
                style={{
                  width: 40,
                  height: 40,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <AntDesign name="left" size={16} color="#1c3587" />
              </View>
            ) : (
              <View
                style={{
                  width: 40,
                  height: 40,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <AntDesign name="right" size={16} color="#1c3587" />
              </View>
            )
          }
          hideExtraDays={true}
          firstDay={1}
          disableAllTouchEventsForDisabledDays={false}
          dayComponent={(current) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  // console.log(
                  //   "🚀 ~ file: ChooseDateScreen.js:106 ~ choosedDate:",
                  //   choosedDate,
                  //   current.date
                  // );

                  setChoosedDate(current.date.dateString);
                }}
                style={{
                  backgroundColor:
                    choosedDate == current.date.dateString
                      ? "#FCA311"
                      : "#f1f1f1",
                  width: 30,
                  height: 30,
                  borderRadius: 5,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text
                  style={[
                    {
                      color:
                        choosedDate == current.date.dateString
                          ? "#fff"
                          : "#14213de6",
                    },
                  ]}
                >
                  {`${current.date.day}`}
                </Text>
              </TouchableOpacity>
            );
          }}
          theme={{
            calendarBackground: "#ffffff",
            textDayHeaderFontWeight: "600",
            monthTextColor: "#14213de6",
            textMonthFontWeight: "600",
            textMonthFontSize: 14,
            dayTextColor: "#14213db3",
            textDayFontWeight: "600",
            textDayFontSize: 12,
          }}
        />
        <TouchableOpacity
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginVertical: 20,
          }}
          onPress={close}
        >
          <Text style={{ color: "#1c3587", fontWeight: "600", fontSize: 14 }}>
            Сонгох
          </Text>
        </TouchableOpacity>
      </View>
    </BlurView>
  );
};

export default ChooseDateScreen;

const styles = StyleSheet.create({
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  buttonClose: {
    backgroundColor: "#1c2090",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginTop: 10,
    width: 120,
  },
});
