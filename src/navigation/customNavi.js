import React, { useState, useContext } from 'react';
import { View, Text, TouchableOpacity, StyleSheet , FlatList, SafeAreaView,LayoutAnimation, Image, Dimensions, useWindowDimensions} from 'react-native';
import Icon from '../component/icon';
import { MaterialCommunityIcons, FontAwesome } from "@expo/vector-icons";
import {EvilIcons} from '@expo/vector-icons'
import { Icons } from '../component/icon';
import { constant } from '../component/constants';
import Wanted from '../../assets/wanted.png'
import Missing from '../../assets/missing.png'
import Prisoner from '../../assets/prisoner.png'
import Illegal from '../../assets/illegal.png'
import Mace from '../../assets/mace.png'
import Devices from '../../assets/devices.png'
import { DataContext } from '../store/DataContext';
const CustomNavi = ({ navigation }) => {
    const dataContext = useContext(DataContext);
  const [ theme, setTheme] = dataContext;
    const [asapd, setAsapd] = useState(false)
    const [isPressed, setIsPressed] = useState(false);
    const [isPresseds, setIsPresseds] = useState(false);
    const [isPresseda, setIsPresseda] = useState(false);
    const [isPressedq, setIsPressedq] = useState(false);
    const [isPressedd, setIsPressedd] = useState(false);
    const [isPressedl, setIsPressedl] = useState(false);
    
    const screenWidth = Dimensions.get('window').width;
  const handlePressIn = () => {
    setIsPressed(true);
  };

  const handlePressOut = () => {
    setIsPressed(false);
  };
  const navigateToScreen = (screenName) => {
    navigation.navigate(screenName);
  };
  const data = [
    { id: '1', screenName: 'Asap5411ABH1', title: '5411 Алга болсон хүн' , type: Icons.Feather, icon: 'home', src: Missing},
    { id: '2', screenName: 'Asap5411SHD1', title: '5411 Шүүхээс даалгасан бусад хүн' ,type: Icons.AntDesign, icon: "user", src: Mace},
    { id: '3', screenName: 'Asap5412Orgodol1', title: '5412 Оргодол' ,type: Icons.AntDesign, icon: "user", src:Prisoner},
    { id: '4', screenName: 'Asap5412GHSE1', title: '5412 Гэмт хэрэгт сэрдэгдсэн этгээд' ,type: Icons.AntDesign, icon: "user", src:Wanted},
    { id: '5', screenName: 'Asap5428Zurchil1', title: '5428 Зөрчил үйлдсэн этгээд' ,type: Icons.AntDesign, icon: "user", src: Illegal},
    { id: '6', screenName: 'Asap5432SmartDevice1', title: '5432 Ухаалаг төхөөрөмж' ,type: Icons.AntDesign, icon: "user", src: Devices},
    // Add more items with screenName corresponding to the screens you want to navigate
  ];
  const dimensions = useWindowDimensions();
    const isLargeScreen = dimensions.width >= 1268;
//console.log("ssssssss",theme)
  const renderItem = ({ item }) => {
    
    //const screenWidth = Dimensions.get('window').width;
    
    return (
      <TouchableOpacity style={[styles.item , isLargeScreen ? null : { width: "100%" } ,]} onPress={() => navigateToScreen(item.screenName)}>
        <Text style={styles.text}>{item.title}</Text>
      </TouchableOpacity>
    );
  };
  return (

    <View style={styles.container}>
      <TouchableOpacity onPress={() => {navigateToScreen('Code'); setIsPresseda(true), setAsapd(false), setIsPressedq(false), setIsPresseds(false), setIsPressedd(false),setIsPressedl(false)}} style={[isPresseda && styles.buttonPressed,styles.touch,isLargeScreen ? null : { width: "100%" }, ]}>
      <MaterialCommunityIcons name="bookshelf" size={24} color="black" style={styles.img}/>
        <Text style={isPresseda ? styles.drawerItems : styles.drawerItem }>Код, журам</Text>
      </TouchableOpacity>
      
      <TouchableOpacity onPressIn={handlePressIn}
        onPressOut={handlePressOut}
        style={[
          styles.touch,
          isLargeScreen ? null : { width: "100%" ,},
          asapd && styles.buttonPressed,   ]}
       
        onPress={()=> {LayoutAnimation.configureNext(LayoutAnimation.create(600, 'easeInEaseOut', 'opacity')) ,setAsapd(!asapd),setIsPresseda(false), setIsPressedq(false), setIsPresseds(false), setIsPressedd(false),setIsPressedl(false)}}>
            <MaterialCommunityIcons
              name="account-search-outline"
              size={24}
              color="black"
              style={styles.img}
            />
        <Text style={asapd ? styles.drawerItems : styles.drawerItem}>Эрэн сурвалжлалт</Text>
      </TouchableOpacity>
      { asapd &&( <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        style={styles.menu}
      /> )}
    
      <TouchableOpacity onPress={() => {navigateToScreen('Zarlan'); setIsPresseda(false), setAsapd(false), setIsPressedq(true), setIsPresseds(false), setIsPressedd(false),setIsPressedl(false)}}  style={[styles.touch, isLargeScreen ? null : { width: "100%" }, isPressedq && styles.buttonPressed, ]}>
      <MaterialCommunityIcons
              name="newspaper-variant-outline"
              size={24}
              color="black"
              style={styles.img}
            />
      
        <Text style={isPressedq ? styles.drawerItems : styles.drawerItem}>Зарлан мэдээлэл</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => {navigateToScreen('Qr'); setIsPresseda(false), setAsapd(false), setIsPressedq(false), setIsPresseds(false), setIsPressedd(true),setIsPressedl(false)}}  style={[styles.touch, isLargeScreen ? null : { width: "100%" }, isPressedd && styles.buttonPressed, ]}>
      <MaterialCommunityIcons
              name="qrcode"
              size={24}
              color="black"
              style={styles.img}
            />
      
        <Text style={isPressedd ? styles.drawerItems : styles.drawerItem}>QR шалгах</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => {navigateToScreen('HumanSearch'); setIsPresseda(false), setAsapd(false), setIsPressedq(false), setIsPresseds(false),setIsPressedd(false), setIsPressedl(true)}} style={[styles.touch,isLargeScreen ? null : { width: "100%" }, isPressedl && styles.buttonPressed,  ]}>
      <FontAwesome
              name="drivers-license-o"
              size={24}
              color="black"
              style={styles.img}
            />
            {/* <Image style={styles.logo} source={require('../../assets/polices.png')}/> */}
        <Text style={isPressedl ? styles.drawerItems : styles.drawerItem}>Хайх</Text>

      </TouchableOpacity>
      <TouchableOpacity onPress={() => {navigateToScreen('Profile'); setIsPresseda(false), setAsapd(false), setIsPressedq(false), setIsPresseds(true),setIsPressedd(false), setIsPressedl(false)}} style={[styles.touch,isLargeScreen ? null : { width: "100%" },  isPresseds && styles.buttonPressed, ]}>

            <Image style={styles.logo} source={require('../../assets/polices.png')}/>
        <Text style={[isPresseds ? styles.drawerItems : styles.drawerItem, // Set text color to white when pressed
      ]}>Хэрэглэгч</Text>
      </TouchableOpacity>
      
    </View>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0,
    alignItems: 'flex-start',
    //justifyContent: 'center',
    paddingTop:40,
   
  },
  drawerItem: {
    fontSize: 16,
    marginVertical: 3,
    margin:25,
    paddingTop:5,
    paddingBottom:5,  
    color: '#000', 
  },
  drawerItems: {
    fontSize: 16,
    marginVertical: 3,
    margin:25,
    color:'#4591D0ff',
    paddingTop:5,
    paddingBottom:5,
  },
  item: {
    paddingHorizontal: constant.SPACING / 1.5,
    paddingVertical: constant.SPACING / 3,
    flexDirection: 'row',
    alignItems: 'center',
    margin:3
     },
  text: {
    fontSize: constant.textFontSize,
    paddingHorizontal: constant.SPACING,
    
  },
  menu: {
    
 
    borderRadius: constant.borderRadius,

    
  },
  image:{
      width:'auto',
      height:'auto'
  },
  button: {
    borderRadius: 4,
  },
  buttonPressed: {
    backgroundColor: '#DFEFFFff',
    borderColor: 'white',
    
    
  },
    buttonPressedWhite: {
    backgroundColor: '#DFEFFFff',
  },
  buttonPressedDark: {
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  buttonDefault: {
    backgroundColor: '#FFF',
  },
  touch: {
    flexDirection:'row',
    alignItems:'center',
    borderRadius:4,
    //marginLeft:10,
    paddingTop:3,
    paddingBottom:3,
    
  },
  img:{
    marginLeft:10,
  },
  logo: {
    width: 26,
    marginLeft:10,
    height: 26,
  },
});

export default CustomNavi;
