import React, { useEffect,useState} from "react";
import { useWindowDimensions } from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { AppState } from 'react-native'; // Import AppState
import AsyncStorage from "@react-native-async-storage/async-storage";
import ProfileScreen from "../screen/user/ProfileScreen";
import SplashScreen from "../screen/SplashScreen";
import LoginScreen from "../screen/user/LoginScreen";
import VerifyScreen from "../screen/user/VerifyScreen";
import CodeScreen from "../screen/codes/CodeScreen";
import CodeRuleScreen from "../screen/codes/CodeRuleScreen";
import SaveListScreen from "../screen/save/SaveListScreen";
import SaveDetailScreen from "../screen/save/SaveDetailScreen";
import SmartCarScreen from "../screen/qr/SmartCarScreen";
import SmartCarDetailScreen from "../screen/qr/SmartCarDetailScreen";
import CodeDetailScreen from "../screen/codes/CodeDetailScreen";
import ZarlanScreen from "../screen/zarlan/ZarlanScreen";
import ZarlanDetail from "../screen/zarlan/ZarlanDetail";
import CovidScreen from "../screen/qr/CovidScreen";
import ZarlanNoti from "../screen/zarlan/ZarlanNoti";
import ChooseDate from "../other/ChooseDateScreen";
import ChooseDateScreen from "../other/ChooseDateScreen";
import AsapDetailScreen from "../screen/asap/AsapDetailScreen";
import Asap5411ABHScreen from "../screen/asap/Asap5411ABHScreen";
import Asap5411SHDScreen from "../screen/asap/Asap5411SHDScreen";
import Asap5412GHSEScreen from "../screen/asap/Asap5412GHSEScreen";
import Asap5412OrgodolScreen from "../screen/asap/Asap5412OrgodolScreen";
import Asap5432SmartDeviceScreen from "../screen/asap/Asap5432SmartDeviceScreen";
import Asap5428ZurchilScreen from "../screen/asap/Asap5428ZurchilScreen";
import Asap5432DetailScreen from "../screen/asap/Asap5432SmartDetail";
import Asap5411HailtScreen from "../screen/asap/Asap5411HailtScreen";
import HumanSearchScreen from "../screen/asap/HumanSearchScreen";
import CustomNavi from "./customNavi";
import deviceStorage from "../store/deviceStorage";
const CodeStack = createStackNavigator();
const SaveStack = createStackNavigator();
const QrStack = createStackNavigator();
const ZarLanStack = createStackNavigator();
const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const horizontalAnimation = {
  cardStyleInterpolator: ({ current, layouts }) => {
    return {
      cardStyle: {
        transform: [
          {
            translateX: current.progress.interpolate({
              inputRange: [0, 1],
              outputRange: [layouts.screen.width, 0],
            }),
          },
        ],
      },
    };
  },
};
const Zarlan = () => {
  return (
    <ZarLanStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="ZarlanList"
    >
      <ZarLanStack.Screen
        name="ZarlanList"
        component={ZarlanScreen}
        options={horizontalAnimation}
      />
      <ZarLanStack.Screen
        name="ZarlanDetail"
        component={ZarlanDetail}
        options={horizontalAnimation}
      />
    </ZarLanStack.Navigator>
  );
};
const Qr = () => {
  return (
    <QrStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="QR"
    >
      <QrStack.Screen
        name="QR"
        component={SmartCarScreen}
        options={horizontalAnimation}
      />
      <QrStack.Screen
        name="QRDetail"
        component={SmartCarDetailScreen}
        options={horizontalAnimation}
      />
      <QrStack.Screen
        name="Covid"
        component={CovidScreen}
        options={horizontalAnimation}
      />
    </QrStack.Navigator>
  );
};
const Code = () => {
  return (
    <CodeStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Category"
    >
      <CodeStack.Screen
        name="Category"
        component={CodeScreen}
        options={horizontalAnimation}
      />
      <CodeStack.Screen
        name="CodeList"
        component={CodeRuleScreen}
        options={horizontalAnimation}
      />
      <CodeStack.Screen
        name="CodeDetail"
        component={CodeDetailScreen}
        options={horizontalAnimation}
      />
    </CodeStack.Navigator>
  );
};
const Save = () => {
  return (
    <SaveStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Save"
    >
      <SaveStack.Screen
        name="Save"
        component={SaveListScreen}
        options={horizontalAnimation}
      />
      <SaveStack.Screen
        name="SaveDetail"
        component={SaveDetailScreen}
        options={horizontalAnimation}
      />
    </SaveStack.Navigator>
  );
};
const Asap5411ABH = () => {
  return (
    <SaveStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Asap5411ABH"
    >
      <SaveStack.Screen
        name="Asap5411ABH"
        component={Asap5411ABHScreen}
        options={horizontalAnimation}
      />
      {/* <SaveStack.Screen
        name="Asap5411ABH"
        component={Asap5411SHDScreen}
        options={horizontalAnimation}
      /> */}
      
      <SaveStack.Screen
        name="AsapDetail"
        component={AsapDetailScreen}
        options={horizontalAnimation}
      />
      <SaveStack.Screen
      name="Asap5411Hailt"
      component={Asap5411HailtScreen}
      options={horizontalAnimation}
      />
    </SaveStack.Navigator>
  );
};

const Asap5411SHD = () => {
  return (
    <SaveStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Asap5411SHD"
    >
      {/* <SaveStack.Screen
        name="Asap"
        component={Asap5411ABHScreen}
        options={horizontalAnimation}
      /> */}
      <SaveStack.Screen
        name="Asap5411SHD"
        component={Asap5411SHDScreen}
        options={horizontalAnimation}
      />
      <SaveStack.Screen
        name="AsapDetail"
        component={AsapDetailScreen}
        options={horizontalAnimation}
      />
    </SaveStack.Navigator>
  );
};
const Asap5412GHSE = () => {
  return (
    <SaveStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Asap5412GHSE"
    >
      {/* <SaveStack.Screen
        name="Asap"
        component={Asap5411ABHScreen}
        options={horizontalAnimation}
      /> */}
      <SaveStack.Screen
        name="Asap5412GHSE"
        component={Asap5412GHSEScreen}
        options={horizontalAnimation}
      />
      <SaveStack.Screen
        name="AsapDetail"
        component={AsapDetailScreen}
        options={horizontalAnimation}
      />
    </SaveStack.Navigator>
  );
};
const Asap5412Orgodol = () => {
  return (
    <SaveStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Asap5412Orgodol"
    >
      {/* <SaveStack.Screen
        name="Asap"
        component={Asap5411ABHScreen}
        options={horizontalAnimation}
      /> */}
      <SaveStack.Screen
        name="Asap5412Orgodol"
        component={Asap5412OrgodolScreen}
        options={horizontalAnimation}
      />
      <SaveStack.Screen
        name="AsapDetail"
        component={AsapDetailScreen}
        options={horizontalAnimation}
      />
    </SaveStack.Navigator>
  );
};
const Asap5428Zurchil = () => {
  return (
    <SaveStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Asap5428Zurchil"
    >
      {/* <SaveStack.Screen
        name="Asap"
        component={Asap5411ABHScreen}
        options={horizontalAnimation}
      /> */}
      <SaveStack.Screen
        name="Asap5428Zurchil"
        component={Asap5428ZurchilScreen}
        options={horizontalAnimation}
      />
      <SaveStack.Screen
        name="AsapDetail"
        component={AsapDetailScreen}
        options={horizontalAnimation}
      />
    </SaveStack.Navigator>
  );
};
const Asap5432SmartDevice= () => {
  return (
    <SaveStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Asap5432SmartDevice"
    >
      {/* <SaveStack.Screen
        name="Asap"
        component={Asap5411ABHScreen}
        options={horizontalAnimation}
      /> */}
      <SaveStack.Screen
        name="Asap5432SmartDevice"
        component={Asap5432SmartDeviceScreen}
        options={horizontalAnimation}
      />
      <SaveStack.Screen
        name="Asap5432Detail"
        component={Asap5432DetailScreen}
        options={horizontalAnimation}
      />
    </SaveStack.Navigator>
  );
};
const RootDrawer = () => {
  const dimensions = useWindowDimensions();
  const isLargeScreen = dimensions.width >= 1268;
  return (
    <Drawer.Navigator
      // openByDefault
      initialRouteName="Code"
      drawerType={isLargeScreen ? "permanent" : "back"}
      drawerStyle={isLargeScreen ? null : { width: "100%" }}
      overlayColor="transparent"
      drawerContent={(props) => <CustomNavi {...props} />}
    >
      <Drawer.Screen
        name="Code"
        component={Code}
        options={{
          headerShown: false,
          drawerLabel: "Код, журам",
          drawerIcon: () => (
            <MaterialCommunityIcons name="bookshelf" size={24} color="black" />
          ),
        }}
      />
      
      <Drawer.Screen
        name="Zarlan"
        component={Zarlan}
        options={{
          headerShown: false,
          drawerLabel: "Зарлан мэдээлэл",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="newspaper-variant-outline"
              size={24}
              color="black"
            />
          ),
        }}
      />
      <Drawer.Screen
        name="Save"
        component={Save}
        options={{
          drawerLabel: "Хадгалсан мэдээлэл",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="content-save"
              size={24}
              color="black"
            />
          ),
        }}
      />
      <Drawer.Screen
        name="Qr"
        component={Qr}
        options={{
          headerShown: false,
          drawerLabel: "QR шалгах",
          drawerIcon: () => (
            <MaterialCommunityIcons name="qrcode" size={24} color="black" />
          ),
        }}
      />
       <Drawer.Screen
        name="Asap5411ABH1"
        component={Asap5411ABH}
        options={{
          headerShown: false,
          drawerLabel: "5411 Алга болсон хүн",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="account-search-outline"
              size={24}
              color="black"
            />
          ),
        }}
      />
      <Drawer.Screen
        name="Asap5411SHD1"
        component={Asap5411SHD}
        options={{
          headerShown: false,
          drawerLabel: "Асапssss",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="account-search-outline"
              size={24}
              color="black"
            />
          ),
        }}
      />
            <Drawer.Screen
        name="Asap5412GHSE1"
        component={Asap5412GHSE}
        options={{
          headerShown: false,
          drawerLabel: "5412 Гэмт хэрэгт сэрдэгдсэн этгээд",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="account-search-outline"
              size={24}
              color="black"
            />
          ),
        }}
      />
                  <Drawer.Screen
        name="Asap5412Orgodol1"
        component={Asap5412Orgodol}
        options={{
          headerShown: false,
          drawerLabel: "5412 Оргодол",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="account-search-outline"
              size={24}
              color="black"
            />
          ),
        }}
      />
                  <Drawer.Screen
        name="Asap5428Zurchil1"
        component={Asap5428Zurchil}
        options={{
          headerShown: false,
          drawerLabel: "5428 Зөрчил үйлдсэн этгээд",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="account-search-outline"
              size={24}
              color="black"
            />
          ),
        }}
      />
                        <Drawer.Screen
        name="Asap5432SmartDevice1"
        component={Asap5432SmartDevice}
        options={{
          headerShown: false,
          drawerLabel: "5432 Ухаалаг төхөөрөмж",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="account-search-outline"
              size={24}
              color="black"
            />
          ),
        }}
      />
      <Drawer.Screen
        name="HumanSearch"
        component={HumanSearchScreen}
        options={{
          headerShown: false,
          drawerLabel: "HumanSearch",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="face-man-profile"
              size={24}
              color="black"
            />
          ),
        }}
      />
      <Drawer.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerShown: false,
          drawerLabel: "Хэрэглэгч",
          drawerIcon: () => (
            <MaterialCommunityIcons
              name="face-man-profile"
              size={24}
              color="black"
            />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};
export default RootNavigation = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  // const navigation = useNavigation();
  // useEffect(() => {
  //   const handleAppStateChange = async (nextAppState) => {
  //     console.log('App State:', nextAppState);
  //     if (nextAppState === 'background') {
  //       // App is going into the background, save the current time
  //       await AsyncStorage.setItem('backgroundTime', Date.now().toString());
  //     } else if (nextAppState === 'active') {
  //       // App is coming back to the foreground, check if 15 minutes have passed
  //       const backgroundTime = await AsyncStorage.getItem('backgroundTime');
  //       console.log("background time",backgroundTime)
  //       const elapsedTime = Date.now() - parseInt(backgroundTime);
  //       console.log("elapsedtime", elapsedTime);
  //       const fifteenMinutes = 1 * 60 * 1000; // 15 minutes in milliseconds
  //       if (elapsedTime >= fifteenMinutes) {
  //         // If more than 15 minutes have passed and the user is logged in, log them out
  //         console.log('15 minutes have passed. Logging out...');
  //         // Perform logout logic here (e.g., clear token, navigate to login screen)
  //         //setIsLoggedIn(false);
  //         console.log("exit");
  //         deviceStorage.deleteStorage().then(() => {
  //           navigation.replace("Splash");
  //                 });
  //       }
  //     }
  //   };
  //   AppState.addEventListener('change', handleAppStateChange);

  //   return () => {
  //     AppState.removeEventListener('change', handleAppStateChange);
  //   };
  // }, []);
  return (
    <NavigationContainer>
      <RootStack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        
        <RootStack.Screen name="Splash" component={SplashScreen} />
        <RootStack.Screen name="Home" options={{ headerShown: false, animation: "slide_from_right" }}component={RootDrawer} />
        <RootStack.Screen name="ZarlanNoti" options={{ headerShown: false, animation: "slide_from_right" }}component={ZarlanNoti} />
        <RootStack.Screen name="Login" options={{ headerShown: false, animation: "slide_from_right" }}component={LoginScreen} />
        <RootStack.Screen name="Verify" options={{ headerShown: false, animation: "slide_from_right" }}component={VerifyScreen} />
        <RootStack.Screen
          name="Date"
          component={ChooseDateScreen}
          options={{ presentation: "modal" }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};
