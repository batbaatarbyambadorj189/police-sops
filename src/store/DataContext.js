import React, { useState, createContext, useContext } from "react";
//Create context object
export const DataContext = createContext();
//Provider data
const dateContext = createContext({
  sawDate: null,
  setCurrentDate: () => {},
});
export const useDate = () => useContext(dateContext);

export const DataContextProvider = (props) => {
  const [data, setData] = useState("");
  const [sawDate, setsawDate] = useState(null);
  const [theme, setTheme] = useState("white");
  const setCurrentDate = (date) => {
    //console.log("setCurrentDate-", date);
    setsawDate(date);
  };
  return (
    <DataContext.Provider value={[data, setData, theme, setTheme]}>
      {props.children}
    </DataContext.Provider>
  );
};
