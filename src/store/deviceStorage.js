import AsyncStorage from "@react-native-async-storage/async-storage";
const deviceStorage = {
  async saveJsonData(key, value) {
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem("@" + key, jsonValue);
    } catch (error) {
      console.log("saveJsonData", error);
      // saving error
    }
  },
  async getJsonData(key) {
    try {
      const result = await AsyncStorage.getItem("@" + key);
      const jsonValue = JSON.parse(result);
      return jsonValue;
    } catch (error) {
       console.log("getJsonData", error);
    }
  },
  async saveData(key, value) {
    try {
      await AsyncStorage.setItem("@" + key, value);
    } catch (error) {
      // saving error
       console.log("saveData", error);
    }
  },
  async getData(key) {
    try {
      const result = await AsyncStorage.getItem("@" + key);
      return result;
    } catch (error) {
      // console.log(error);
       console.log("getData", error);
    }
  },
  async getUser() {
    try {
      const result = await AsyncStorage.getItem("@user");
      const jsonValue = JSON.parse(result);
      return jsonValue;
    } catch (error) {
       console.log("getUser", error);
    }
  },
  async loginSave(value) {
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem("@login", jsonValue);
    } catch (error) {
      console.log("saveJsonData", error);
      // saving error
    }
  },
  async getLoginSave() {
    try {
      const result = await AsyncStorage.getItem("@login");
      const jsonValue = JSON.parse(result);
      return jsonValue;
    } catch (error) {
       console.log("getJsonData", error);
    }
  },
  async deleteStorage() {
    // LOG ? console.log('deleteStorage'):''
    try {
      // console.log("deleteStorage");
      // let login;
      // this.getLoginSave().then((res) => {
      //   login = res;
      // });
      await AsyncStorage.clear().then(() => {
        // this.loginSave(login);
        return true;
      });
    } catch (error) {
      // console.log("AsyncStorage Error: " + error.message);
    }
  },
};
export default deviceStorage;
