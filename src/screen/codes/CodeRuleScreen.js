import React, { useContext, useState, useEffect } from "react";
import {
  View,
  Text,
  Alert,
  FlatList,
  StyleSheet,
  TextArea,
  ImageBackground,
  ActivityIndicator,
  Platform,
  useWindowDimensions,
} from "react-native";
import axios from "axios";
import { TouchableHighlight, TextInput } from "react-native-gesture-handler";
import CodeItem from "../../component/code/codeItem";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { DataContext } from "../../store/DataContext";
const CodeRuleScreen = (datas) => {
  const BaseUrl = "http://www.rule.police.gov.mn"
  const navigation = useNavigation();
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const [loading, setLoading] = useState(true);
  const [list, setList] = useState([]);
  const [resultList, setResultList] = useState([]);
  const [isShowInput, setIsShowInput] = useState(false);
  const [search, setSearch] = useState();
  const { title, id } = datas.route.params;
  const dimensions = useWindowDimensions();
  const getData = async () => {
    // console.log('====================================');
    // console.log('id',id);
    // console.log('====================================');
    axios({
      method: "POST",
      url: `/police/getCodeList`,
      data: { ruleId: id },
    })
      .then((res) => {
        // process.env.NODE_ENV === "development" && console.log('====================================');
        // console.log(res.data);
        // console.log('====================================');
        setLoading(false);
        setList(res.data);
      })
      .catch((err) => {
        // console.log('====================================');
        // console.log('err',err);
        // console.log('====================================');
        setLoading(false);
      });
  };
  useEffect(() => {
    let clean = getData();
    return () => {
      clean;
    };
  }, []);

  return (
    <View style={styles.container}>
      <View
        style={[
          styles.notch,
          theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.8)" },
        ]}
      />
      <View
        style={[
          styles.colorContainer,
          theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.8)" },
        ]}
      >
        <View style={{ flexDirection: "row" }}>
          <TouchableHighlight
            onPress={() => {
              navigation.goBack();
            }}
          >
            <MaterialCommunityIcons
              size={30}
              color="white"
              name="arrow-left"
              style={styles.icon}
            />
          </TouchableHighlight>
          {!isShowInput ? (
            <TouchableHighlight
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Text
                style={[styles.title, { maxWidth: dimensions.width - 100 }]}
              >
                {title}
              </Text>
            </TouchableHighlight>
          ) : (
            <TextInput
              style={{
                width: dimensions.width - 110,
                color: "white",
                fontSize: 20,
                fontWeight: "400",
              }}
              value={search}
              autoFocus={true}
              onChangeText={(text) => {
                let arr = list;
                // console.log('text',text);
                let result = [];
                arr.map((item) => {
                  item?.RULETITLE?.toLowerCase().search(text.toLowerCase()) !=
                  "-1"
                    ? result.push(item)
                    : "";
                });
                setResultList(result);
                setSearch(text.toLowerCase());
              }}
            />
          )}
        </View>
        <TouchableHighlight
          onPress={() => {
            setIsShowInput((prev) => !prev);
          }}
        >
          <MaterialCommunityIcons
            size={30}
            color="white"
            name={!isShowInput ? "magnify" : "close"}
            style={styles.icon}
          />
        </TouchableHighlight>
      </View>
      {loading ? (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            alignSelf: "center",
          }}
        >
          <ActivityIndicator size="large" color="#1c3587" />
        </View>
      ) : (
        <View style={{ flex: 1 }}>
          <FlatList
            data={isShowInput ? resultList : list}
            renderItem={({ item, index }) => <CodeItem item={item} />}
            keyExtractor={(item) => item.ID.toString()}
          />
        </View>
      )}
    </View>
  );
};
export default CodeRuleScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    flex: 1,
    textAlign: "left",
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    alignItems: "flex-start",
  },
  colorContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#1c3587",
    height: 60,
    paddingTop: 10,

    maxHeight: 60,
    minHeight: 60,
    // marginTop: isNotch ? 32 : 0
  },
  icon: {
    margin: 10,
  },
  title: {
    fontSize: 20,
    paddingTop: 14,
    color: "white",
    fontWeight: "500",
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
  notch: {
    backgroundColor: "#1c3587",
    height: Platform === "ios" ? 40 : 28,
  },
});
