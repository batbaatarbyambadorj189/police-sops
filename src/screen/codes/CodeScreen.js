import React, { useState, useEffect, useContext,useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
  Platform,
  Alert,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import CodeList from "../../component/code/codeList";
import Header from "../../component/header";
import { DataContext } from "../../store/DataContext";
import axios from "axios";
import Services from "./Services";
import AsyncStorage from "@react-native-async-storage/async-storage";
//import { AppState } from 'react-native';
import { AppState } from "react-native";
import * as Device from 'expo-device';
import deviceStorage from "../../store/deviceStorage";
import * as Notifications from 'expo-notifications';
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});
const CodeScreen = () => {
  const BaseUrl ="http://www.rule.police.gov.mn"
  const dataContext = useContext(DataContext);
  const [data, setData] = dataContext;
  const [loading, setLoading] = useState(true);
  const [list, setList] = useState([]);
  const navigation = useNavigation();
  const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  const getData = async () => {
    try {
      setLoading(true);
       //console.log("data-code", data.token);
      await axios({
        url: `/police/getCodeList`,
        method: "POST",
        data: { ruleId: "" },
      })
        .then((res) => {
          //console.log("res code", res.data);
          setLoading(false);
          setList(res.data);
        })
        .catch((err) => {
          setLoading(false);
          //console.log("err code", err.message);
        });
    } catch (err) {"catch err", err}
  };
  // useEffect(() => {
  //   let clean = getData();
  //   return () => {
  //     clean;
  //   };
  // }, []);
  useEffect(() => {
    let clean = getData();
    
    registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      //console.log(response);
    });


    const handleAppStateChange = async (nextAppState) => {
      //console.log('App State:', nextAppState);
      if (nextAppState === 'background') {
        // App is going into the background, save the current time
        await AsyncStorage.setItem('backgroundTime', Date.now().toString());
      
      } else if (nextAppState === 'active') {
        // App is coming back to the foreground, check if 15 minutes have passed
        const backgroundTime = await AsyncStorage.getItem('backgroundTime');
        //console.log("background time",backgroundTime)
        const elapsedTime = Date.now() - parseInt(backgroundTime);
        //console.log("elapsedtime", elapsedTime);
        const fifteenMinutes = 15 * 60 * 1000; // 15 minutes in milliseconds
        if (elapsedTime >= fifteenMinutes) {
          // If more than 15 minutes have passed and the user is logged in, log them out
          //console.log('15 minutes have passed. Logging out...');
          // Perform logout logic here (e.g., clear token, navigate to login screen)
          //setIsLoggedIn(false);
          //console.log("exit");
          // await Notifications.scheduleNotificationAsync({
          //   content: {
          //     title: 'App logout',
          //     body: 'The app has entered background mode.',
          //   },
          //   trigger: null, // Send immediately
          // });
          // call the async () => {
          //await schedulePushNotification()
          Alert.alert("Та идэвхгүй 15 минут болсон тул холболт саллаа.")
          deviceStorage.deleteStorage().then(() => {
            navigation.replace("Splash");
          });
        }
      }
    };
    AppState.addEventListener('change', handleAppStateChange);

    return () => {
      clean;   
      AppState.removeEventListener('change', handleAppStateChange);


      Notifications.removeNotificationSubscription(notificationListener.current);
       Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  const schedulePushNotification = async() => {
    await Notifications.scheduleNotificationAsync({
      content: {
        title: "You've got mail! 📬",
        body: 'Here is the notification body',
        data: { data: 'goes here' },
      },
      trigger: { seconds: 2 },
    });
  }
  
  const registerForPushNotificationsAsync = async() =>{
    let token;
  
    if (Platform.OS === 'android') {
      await Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
  
    if (Device.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      // Learn more about projectId:
      // https://docs.expo.dev/push-notifications/push-notifications-setup/#configure-projectid
      token = (await Notifications.getExpoPushTokenAsync({ projectId: '0b6604ab-6810-4786-bcc8-31287e1b3394' })).data;
      //console.log(token);
    } else {
      alert('Must use physical device for Push Notifications');
    }
  
    return token;
  }
  return (
    <View style={styles.container}>
      <Header title={"Код, журам"} type={"menu"} /> 
      {loading ? (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            alignSelf: "center",
          }}
        >
          <ActivityIndicator size="large" color="#1c3587" />
        </View>
      ) : (
        <View style={{ flex: 1 }}>
          <FlatList
            data={list}
            renderItem={({ item, index }) => (
              <CodeList item={item} key={index} type={"group"} />
            )}
            keyExtractor={(item, index) => item.RULETYPE_ID.toString()}
          />
          {/* <Services/> */}
        </View>
      )} 
    </View>
  );
};

export default CodeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
});
