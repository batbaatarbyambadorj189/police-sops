import { StyleSheet,Text, View , ScrollView, Pressable,Image, FlatList, TouchableOpacity } from "react-native";
import React from "react";
import objectPath from "object-path";
  const Services =()=>{
const services = [

  ];
  return(
    
    <View style={{margin:20,padding:10, marginTop:-70, backgroundColor:"white", borderRadius:24}}>
      
      <Text style={{fontSize:16, fontWeight:'500', marginBottom:7,paddingTop:10}}>Хадгалсан</Text>
      <FlatList  ListEmptyComponent={<Text>Empty</Text>}  showsHorizontalScrollIndicator={false}  
      
        data={services}
        renderItem={({item,index})=>{
        const image =   objectPath.get(item,"image")
        const name =   objectPath.get(item,"name")
       return   <TouchableOpacity  style={{margin:10,  backgroundColor:'white', padding:20, borderRadius:4, maxWidth:350,borderWidth:1,shadowColor:'#00000040',shadowOpacity: 0.8,
       shadowRadius: 4,
       shadowOffset: {
         height: 1,
         width: 1
       }}} key={index}>
          {/* <Image source={{uri:image}} resizeMode="cover" style={{width:50, height:50, alignSelf:'center'}}/> */}
          <Text style={{maxWidth:300, textAlign:'center',fontSize:12}}>{name}</Text>
        </TouchableOpacity>
        }}
      
      />
       
     
    </View>
  )
  }
  export default Services;

  export function getProduct(id){
    return Services.find((product) => product.id == id);
}