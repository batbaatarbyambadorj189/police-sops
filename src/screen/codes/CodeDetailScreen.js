import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import Header from "../../component/header";
import { WebView } from "react-native-webview";
const CodeDetailScreen = (data) => {
  let { item } = data.route.params;

  const savePDF = () => {};
  return (
    <>
      <View style={styles.container}>
        <Header
          title={
            (
              item?.RULETITLE?.charAt(0).toUpperCase() +
              item?.RULETITLE?.slice(1)
            ).substring(0, 20) + "..."
          }
          type={"back"}
          isSave={false}
          functions={savePDF}
        />
        {item?.GDRIVE === "" ? (
          <View
            style={{
              ...StyleSheet.absoluteFillObject,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text>Мэдээлэл олдсонгүй.!!!</Text>
          </View>
        ) : (
          <WebView
            source={{
              uri: item?.GDRIVE,
            }}
          />
        )}
      </View>
    </>
  );
};

export default CodeDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
