import React, { useState, useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import {
  SafeAreaView,
  Text,
  StyleSheet,
  ScrollView,
  View,
  Image,
  ActivityIndicator,
  TouchableHighlight,
  Pressable,
} from "react-native";
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from "react-native-confirmation-code-field";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import axios from "axios";
import { DataContext } from "../../store/DataContext";

const VerifyScreen = (datas) => {
  const BaseUrl ="http://www.rule.police.gov.mn"
  const navigation = useNavigation();
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const [value, setValue] = useState("");
  const [loading, setLoading] = useState(true);
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  const CELL_COUNT = 6;
  const check = async () => {
    try {
      setLoading(true);
      let { params } = datas.route;
      let { username, phone, privacyNumber } = params;
      let res = await axios({
        url: `/police/checkMSGPassword`,
        method: "POST",
        data: {
          msgToken: value,
          username,
          phone,
          privacyNumber,
        },
      });

      if (res.data == "Мэдээлэл олдсонгүй!") {
        this._showAlert("Мэдээлэл", res.data);
        setLoading(false);
      } else {
        let data = {
          token: res?.data?.token,
          user: res?.data?.user,
        };
        axios.defaults.headers.common["Authorization"] =
          "Bearer " + res.data.token;
        setData(data);
        setLoading(false);
        navigation.replace("Home");
      }
    } catch (err) {}
  };
  return (
    <ScrollView style={styles.scroll}>
      <View style={styles.container}>
        <View
          style={[
            styles.header,
            theme === "white" ? {} : { backgroundColor: "rgb(0,0,0,0.4)" },
          ]}
        >
          <Text style={styles.titleStyle}>Баталгаажуулах</Text>
          <Image
            source={require("../../../assets/email.png")}
            style={styles.iconStyle}
          />
        </View>
        <View style={styles.root}>
          <CodeField
            ref={ref}
            {...props}
            // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
            value={value}
            onChangeText={setValue}
            cellCount={CELL_COUNT}
            rootStyle={styles.codeFieldRoot}
            keyboardType="number-pad"
            textContentType="oneTimeCode"
            renderCell={({ index, symbol, isFocused }) => (
              <Text
                key={index}
                style={[styles.cell, isFocused && styles.focusCell]}
                onLayout={getCellOnLayoutHandler(index)}
              >
                {symbol || (isFocused ? <Cursor /> : null)}
              </Text>
            )}
          />
        </View>
        {loading ? (
          <Pressable
            onPress={() => {
              check();
            }}
            style={styles.buttonStyle}
          >
            <MaterialCommunityIcons
              style={{ paddingLeft: 25 }}
              name="arrow-right-thick"
              size={36}
              color="white"
            />
          </Pressable>
        ) : (
          <ActivityIndicator size="large" color="#1c3587" />
        )}
      </View>
    </ScrollView>
  );
};

export default VerifyScreen;

const styles = StyleSheet.create({
  buttonStyle: {
    width: 80,
    height: 80,
    backgroundColor: "#1c3587",
    justifyContent: "center",
    borderRadius: 80,
    alignSelf: "center",
    marginTop: 30,
  },
  iconStyle: {
    width: 80,
    height: 80,
    marginTop: 40,
    alignSelf: "center",
  },
  titleStyle: {
    alignSelf: "center",
    marginTop: 90,
    color: "#FFF",
    fontSize: 28,
    fontWeight: "700",
  },
  header: {
    backgroundColor: "#1c3587",
    height: 200,
    alignContent: "center",
  },
  container: {
    flexDirection: "column",
  },
  scroll: { flex: 1 },
  root: { flex: 1, padding: 50, marginTop: 50 },
  title: { textAlign: "center", fontSize: 30 },
  codeFieldRoot: { padding: 20 },
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 24,
    borderWidth: 2,
    borderColor: "#00000030",
    textAlign: "center",
  },
  focusCell: {
    borderColor: "#1c3587",
  },
});
