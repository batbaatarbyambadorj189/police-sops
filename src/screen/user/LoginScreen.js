import {
  ImageBackground,
  Platform,
  SafeAreaView,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Text,
  View,
  Alert,

  TextInput,

  KeyboardAvoidingView,
} from "react-native";
import React, { useState, useContext, useEffect, useCallback } from "react";
import { useNavigation, StackActions } from '@react-navigation/native'
import { MaterialCommunityIcons } from "@expo/vector-icons";
import deviceStorage from "../../store/deviceStorage";
import Logo from "../../component/logo";
import axios from "axios";
import * as Device from "expo-device";
import * as LocalAuthentication from "expo-local-authentication";
import { DataContext } from "../../store/DataContext";
const LoginScreen = (datas) => {
 
  const BaseUrl ="http://www.rule.police.gov.mn"
  const {dispatch} = useNavigation();
  const dataContext = useContext(DataContext);
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState("007021002");
  const [phone, setPhone] = useState("80280203");
  const [privacyNumber, setPrivacyNumber] = useState("17111");
  const [isSave, setIsSave] = useState(false);
  const [data, setData] = dataContext;
  let {token} = datas.route.params;
console.log('tokem',datas.route)


const auth = async (phone, username, privacyNumber) => {
  try {
    //setLoading(true);
    // console.log("auth");
    let hasHard = await LocalAuthentication.hasHardwareAsync();
    // console.log("hard", hasHard);
    if (hasHard && Platform.OS === "ios") {
      let result = await LocalAuthentication.authenticateAsync();
      if (result.success) {
        // console.log('did',username);
        // console.log('did',phone);
        // console.log('did',privacyNumber);
        let a = {
          privacyNumber,
          username: "pu" + username,
          phone,
          os: Platform.OS === "ios" ? "IOS" : "ANDROID",
          mark: Device.brand,
          model: Device.modelName,
        };
        // console.log("====================================");
        // console.log("did", axios.defaults.baseURL);
        // console.log("====================================");
        await axios({
          url: `/police/login`,
          method: "POST",

          data: {
            privacyNumber,
            username: "pu" + username,
            phone,
            os: Platform.OS === "ios" ? "IOS" : "ANDROID",
            mark: Device.brand,
            model: Device.modelName,
            // device_id: Constants.deviceId,
          },
        })
          .then((res) => {
            // console.log('====================================');
            // console.log("res", res.data);
            // console.log('====================================');
            if (res.data.state == "ok") {
              // console.log('====================================');
              // console.log('did');
              // console.log('====================================');
              if (res.data.result == "main") {
                let data = {
                  token: res.data.token,
                  user: res.data.user,
                };
                // console.log("jwt", res.data.token);
                axios.defaults.headers.common["Authorization"] =
                  "Bearer " + res.data.token;
                setData(data);
                setLoading(false);
                // console.log("===========", res?.data?.user?.DEPTCODE);
                axios.post(
                  "https://us-central1-mn-npa-sop.cloudfunctions.net/saveMsgToken",
                  {
                    username: "pu" + username,
                    token: token,
                    deptcode: res?.data?.user?.DEPTCODE,
                  }
                );
                dispatch(StackActions.push("Home"));
              } else {
                setLoading(false);
                dispatch(StackActions.replace("Verify", {
                  username,
                  phone,
                  privacyNumber,
                }));
              }
            }
          })
          .catch((err) => {
            // console.log('====================================');
            // console.log("err", err.message);
            // console.log('====================================');
          });
      } else setLoading(false);
    } else {
      await axios({
        url: `/police/login`,
        method: "POST",

        data: {
          privacyNumber,
          username: "pu" + username,
          phone,
          os: Platform.OS === "ios" ? "IOS" : "ANDROID",
          mark: Device.brand,
          model: Device.modelName,
          // device_id: Constants.deviceId,
        },
      })
        .then((res) => {
          // console.log('====================================');
          // console.log("res", res.data);
          // console.log('====================================');
          if (res.data.state == "ok") {
            // console.log('====================================');
            // console.log('did');
            // console.log('====================================');
            if (res.data.result == "main") {
              let data = {
                token: res.data.token,
                user: res.data.user,
              };
              // console.log("jwt", res.data.token);
              axios.defaults.headers.common["Authorization"] =
                "Bearer " + res.data.token;
              setData(data);
              // console.log("===========", res?.data?.user?.DEPTCODE);
              axios.post(
                "https://us-central1-mn-npa-sop.cloudfunctions.net/saveMsgToken",
                {
                  username: "pu" + username,
                  token: token,
                  deptcode: res?.data?.user?.DEPTCODE,
                }
              );
              dispatch(StackActions.push("Home"));
            } else {
              setLoading(false);
              dispatch(StackActions.replace("Verify", {
                username,
                phone,
                privacyNumber,
              }));
            }
          }
        })
        .catch((err) => {
          // console.log('====================================');
          //console.log("err", err.message);
          Alert.alert("");
          // console.log('====================================');
        });
    }
  } catch (err) {}
};
const authFirst = async () => {
  let hasHard = await LocalAuthentication.hasHardwareAsync();
   //console.log("authFirst");
   //console.log("hashHard", hasHard);
   const isEnrolled = await LocalAuthentication.isEnrolledAsync();
  let result = await LocalAuthentication.authenticateAsync();
  //console.log("result",result,isEnrolled);
  if (result.success) dispatch(StackActions.replace("Home"));
};
useEffect(() => {
  try {
    //console.log("useEffect");
    setLoading(true);
    deviceStorage.getLoginSave().then((res) => {
      if (res != null) {
        //console.log("asdasd", res.privacyNumber);
       // console.log("data", res.username);
        //console.log("setData", res.phone);
        setPhone(res.phone);
        setPrivacyNumber(res.privacyNumber);
        setUsername(res.username);
        setIsSave(true);
        auth(res.phone, res.username, res.privacyNumber);
      } else setLoading(false);
    });
  } catch (err) {}
  return () => {};
}, []);
const renderSpinner=()=>{
  <View style={styles.spinnerStyle}>
    <ActivityIndicator color={"#FFF"} size="large" />
  </View>
  
}
const _showAlert = (title, description) => {
  Alert.alert(title, description, [{ text: "Хаах" }], {
    cancelable: false,
  });
};

  // const login = async() =>{
  //     setLoading(true)

  //             let res = await axios({ 
  //               method: 'POST',
  //               url: `/police/login`,

  //               data: {
  //                 privacyNumber,
  //                 username: "pu" + username,
  //                 phone,
  //                 os: Platform.OS === "ios" ? "IOS" : "ANDROID",
  //                 mark: Device.brand,
  //                 model: Device.modelName,
  //                 // device_id: Constants.deviceId,
  //               },
  //             })
  //             setLoading(false);
  //             console.log("sfsfdsf", res.data.state, res.data.result, res.data.token)
  //             if (res.data.state == "ok") {
  //               if (res.data.result == "main") {
  //                 setLoading(false);
      
  //                 let data = {
  //                   token: res.data.token,
  //                   user: res.data.user,
  //                 };
  //                 console.log("token", res.data.token);
  //                 axios.defaults.headers.common["Authorization"] =
  //                   "Bearer " + res.data.token;
  //                 setData(data);
  //                 console.log("===========", res?.data?.user?.DEPTCODE);
  //                 axios.post(
  //                   "https://us-central1-mn-npa-sop.cloudfunctions.net/saveMsgToken",
  //                   {
  //                     username: "pu" + username,
  //                     token: token,
  //                     deptcode: res?.data?.user?.DEPTCODE,
  //                   }
  //                 );
  //                 console.log("isSave", isSave);
  //                 if (isSave) {
  //                   let data = {
  //                     phone,
  //                     username,
  //                     privacyNumber,
  //                   };
  //                   deviceStorage.loginSave(data);
  //                   authFirst();
  //                 } else {
  //                   setLoading(false);
  //                   dispatch(StackActions.replace("Home"));
  //                 }
  //               } else {
  //                 setLoading(false);
  //                 dispatch(StackActions.replace("Verify", {
  //                   username,
  //                   phone,
  //                   privacyNumber,
  //                 }));
  //               }
  //             } else {
  //               setLoading(false);
  //               // console.log("rs", res.data);
  //               res.data.error
  //                 ? _showAlert("Алдаа", res.data.error)
  //                 : _showAlert("Алдаа", res.data);
  //             }
  //             //dispatch(StackActions.replace("Home"))
  // }

  const login = async () => {
    try {
      if (username != null && phone != null && privacyNumber != null) {
        setLoading(true);
        // console.log("asdasd", privacyNumber);
        // console.log("data", username);
        // console.log("setData", phone);
        console.log("dddddddd");
        let res = await axios({
          url: `/police/login`,
          method: "POST",
          data: {
            privacyNumber,
            username: "pu" + username,
            phone,
            os: Platform.OS === "ios" ? "IOS" : "ANDROID",
            mark: Device.brand,
            model: Device.modelName,
            // device_id: Constants.deviceId,
          },
        });
        console.log("res", res.data);
        if (res.data.state == "ok") {
          if (res.data.result == "main") {
            setLoading(false);

            let data = {
              token: res.data.token,
              user: res.data.user,
            };
            // console.log("token", res.data.token);
            axios.defaults.headers.common["Authorization"] =
              "Bearer " + res.data.token;
            setData(data);
            // console.log("===========", res?.data?.user?.DEPTCODE);
            axios.post(
              "https://us-central1-mn-npa-sop.cloudfunctions.net/saveMsgToken",
              {
                username: "pu" + username,
                token: token,
                deptcode: res?.data?.user?.DEPTCODE,
              }
            );
            // console.log("isSave", isSave);
            if (isSave) {
              let data = {
                phone,
                username,
                privacyNumber,
              };
              deviceStorage.loginSave(data);
              authFirst();
            } else {
              setLoading(false);
              dispatch(StackActions.replace("Home"));
            }
          } else {
            setLoading(false);
            dispatch(StackActions.replace("Verify", {
              username,
              phone,
              privacyNumber,
            }));
          }
        } else {
          setLoading(false);
          // console.log("rs", res.data);
          res.data.error
            ? _showAlert("Алдаа", res.data.error)
            : _showAlert("Алдаа", res.data);
        }
      } else {
        _showAlert(
          "Анхааруулга",
          "Нэвтрэх нэр, Хувийн дугаар, Утасны дугаараа бүрэн оруулна уу.!!!"
        );
      }
    } catch (err) {
      console.log('err',err);
      _showAlert("Алдаа", err);
    }
  };

  const renderLoginButton = () => (
    <TouchableOpacity
      style={styles.loginButtonStyle}
      onPress={() => {
        login();
      }}
    >
      <Text style={styles.loginButtonTextStyle}>НЭВТРЭХ</Text>
    </TouchableOpacity>
  );
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ backgroundColor: "#000", flex: 1 }}
    >
      <ImageBackground
        style={styles.bimage}
        imageStyle={{ borderBottomRightRadius: 50, borderBottomLeftRadius: 50 }}
        source={require("../../../assets/background.png")}
        resizeMode="cover"
      >
        <View style={styles.container}>
          <View style={styles.blackoverlay}>
            <View style={styles.loginContainer}>
              <Logo />
              <View style={styles.inputContainer}>
                <View style={{ marginTop: 10 }} />
                <View style={styles.cardContainer}>
                  <View style={styles.cardContainerGlue}>
                    <MaterialCommunityIcons
                      name="account"
                      style={{ marginLeft: 1 }}
                      size={35}
                      color="rgba(0,0,0,1)"
                    />
                    <View
                      style={(styles.cardTextContainer, { marginLeft: 10 })}
                    >
                      <Text style={styles._textStyle}>Нэвтрэх нэр</Text>
                      <TextInput
                        placeholder="001011010"
                        maxLength={9}
                        keyboardType="number-pad"
                        value={username}
                        returnKeyType={Platform.OS === "ios" ? "done" : "next"}
                        onChange={(text) => {
                          setUsername(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                  </View>
                </View>
                <View style={styles.cardContainer}>
                  <View style={styles.cardContainerGlue}>
                    <MaterialCommunityIcons
                      name="card-account-details-star"
                      style={{ marginTop: 5 }}
                      size={35}
                      color="rgba(0,0,0,1)"
                    />
                    <View style={styles.cardTextContainer}>
                      <Text style={styles._textStyle}>Хувийн дугаар</Text>
                      <TextInput
                        placeholder="12345"
                        keyboardType="number-pad"
                        name="privacyNo"
                        maxLength={5}
                        returnKeyType={Platform.OS === "ios" ? "done" : "next"}
                        value={privacyNumber}
                        onChange={(text) => {
                          setPrivacyNumber(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                  </View>
                </View>
                <View style={styles.cardContainer}>
                  <View style={styles.cardContainerGlue}>
                    <MaterialCommunityIcons
                      name="cellphone-check"
                      size={35}
                      style={{ marginLeft: 1 }}
                      color="rgba(0,0,0,1)"
                    />
                    <View
                      style={(styles.cardTextContainer, { marginLeft: 15 })}
                    >
                      <Text style={styles._textStyle}>Утасны дугаар</Text>
                      <TextInput
                        placeholder="99887766"
                        name="phone"
                        keyboardType="number-pad"
                        maxLength={8}
                        value={phone}
                        returnKeyType="done"
                        onChange={(text) => {
                          setPhone(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                  </View>
                </View>
                <TouchableOpacity
                  style={styles.checkboxContainer}
                  onPress={() => {
                    setIsSave((prevIsSave) => !prevIsSave);
                  }}
                >
                  {isSave ? (
                    <MaterialCommunityIcons
                      name="checkbox-marked"
                      size={24}
                      color="black"
                    />
                  ) : (
                    <MaterialCommunityIcons
                      name="checkbox-blank-outline"
                      size={24}
                      color="black"
                    />
                  )}

                  <Text style={styles.label}>Сануулах</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </ImageBackground>
      {loading ? renderSpinner() : renderLoginButton()}
    </KeyboardAvoidingView>
  )
}

export default LoginScreen


const styles = StyleSheet.create({
  label: {
    margin: 3,
    marginRight: 15,
    fontStyle: "normal",
    fontSize: 15,
    fontWeight: "bold",
  },
  checkboxContainer: {
    flexDirection: "row",
    alignSelf: "flex-end",
  },
  container: {
    flex: 1,
    marginTop: Platform === "ios" ? 32 : 28,
  },

  bimage: {
    flex: 1,
    zIndex: -1,
    ...StyleSheet.absoluteFillObject,
    marginBottom: 100,
  },
  spinnerStyle: {
    left: 0,
    right: 0,
    zIndex: 9,
    height: 40,
    bottom: 40,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  loginButtonStyle: {
    left: 0,
    right: 0,
    zIndex: 9,
    height: 80,
    bottom: 20,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  loginButtonTextStyle: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
    marginBottom: 15,
  },
  blackoverlay: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "rgba(0,0,0,0.1)",
  },
  loginContainer: {
    flex: 1,
  },
  inputContainer: {
    height: 300,
    bottom: 125,
    backgroundColor: "rgba(255,255,255,0.2)",
    borderRadius: 24,
    width: "90%",
    position: "absolute",
    left: "5%",
    bottom: 20,
  },
  cardContainerGlue: {
    flex: 1,
    marginLeft: 24,
    marginRight: 24,
    alignItems: "center",
    flexDirection: "row",
  },
  cardTextContainer: {
    flex: 1,
    marginLeft: 12,
    flexDirection: "column",
    justifyContent: "center",
    marginTop: Platform === "ios" ? null : 10,
  },
  cardContainer: {
    margin: 8,
    height: 75,
    width: "95%",
    marginTop: 0,
    borderRadius: 24,
    justifyContent: "center",
    backgroundColor: "white",
  },
  _textInputStyle: {
    fontSize: 17,
    color: "#000",
    fontWeight: "900",
    width: "100%",
    minWidth: "70%",
  },
  _InputStyle: {
    fontSize: 14,
    color: "#000",
    fontWeight: "800",
  },
  _textStyle: {
    fontSize: 14,
    fontWeight: "700",
    color: "#000",
    // backgroundColor:'yellow',
  },
});