import React, { useEffect, useContext } from "react";
import { StyleSheet, Text, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Header from "../../component/header";
import { DataContext } from "../../store/DataContext";

var dateFormat = require("dateformat");

const ZarlanNoti = (datas) => {
  // console.log("datas", datas);
  const { item } = datas.route.params;
  // console.log("item", item);
//console.log("zereglel",item)
  return (
    <View style={{ flex: 1 }}>
      <Header title={"Зарлан дэлгэрэнгүй"} type={"back"} />
      <ScrollView>
        <View style={{ padding: 12, flexDirection: "column", flex: 1 }}>
          <View style={styles.row}>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {item?.ZARLAN_TYPE_NAME}
            </Text>
          </View>
          <View style={styles.row}>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {item?.CREATEDDATE}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Утга</Text>
            <Text style={{ textAlign: "justify" }}>{item?.CONTENT}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Зэрэглэл</Text>
            {item?.ZEREGLEL_ID == "1" ? (
              <Text style={{ color: "#f20039", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME}
              </Text>
            ) : item?.ZEREGLEL_ID === "2" ? (
              <Text style={{ color: "#ff543d", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME}
              </Text>
            ) : item?.ZEREGLEL_ID === "3" ? (
              <Text style={{ color: "#ff853d", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME}
              </Text>
            ) : item?.ZEREGLEL_ID === "4" ? (
              <Text style={{ color: "#C3941A", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME}
              </Text>
            ) : (
              <View />
            )}
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Мэдээллэх хүрээ</Text>
            <Text style={{ textAlign: "justify" }}>{item?.HUREE_NAME}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Анхаарах зүйлс</Text>
            <Text style={{ textAlign: "justify" }}>{item?.ANHAARAH}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Холбогдох үйл явдал</Text>
            <Text style={{ textAlign: "justify" }}>{item?.EVENT_NAME}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Зарласан</Text>
            <Text style={{ textAlign: "justify" }}>{item?.SHORTNAME}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Байршлын мэдээлэл</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.AIMAG_NAME +
                "," +
                item?.SUM_NAME +
                " " +
                item?.HOROO +
                " баг/хороо \nХаяг : " +
                item?.ADDRESS}
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default ZarlanNoti;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#Fff",
  },
  item: {
    flex: 1,
    textAlign: "left",
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    alignItems: "flex-start",
  },
  colorContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#788eec",
    height: 50,
    maxHeight: 50,
    minHeight: 50,
    // marginTop: isNotch ? 32 : 0
  },
  icon: {
    margin: 10,
  },
  title: {
    fontSize: 16,
    color: "black",
    fontWeight: "700",
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
  row: {
    paddingTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  minicon: {
    flexDirection: "column",
    paddingTop: 3,
  },
});
