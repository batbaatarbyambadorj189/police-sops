import React, {
  useState,
  useContext,
  useEffect,
  useCallback,
  useLayoutEffect,
} from "react";
import {
  StyleSheet,
  Text,
  View,
  Modal,
  useWindowDimensions,
  Pressable,
  Button,
  ActivityIndicator,
  FlatList,
  LogBox,
  Alert,
} from "react-native";
import Header from "../../component/header";
import dateFormat from "dateformat";
import { DataContext, useDate } from "../../store/DataContext";
import axios from "axios";
import Zarlan from "../../component/zarlan/zarlan";

import { StackActions, useNavigation } from "@react-navigation/native";
const ZarlanScreen = ({ route }) => {
  const currentdate = route?.params?.currentdate;
  const dataContext = useContext(DataContext);

  const [data, setData, theme, setTheme] = dataContext;
 
 
  const [date, setDate] = useState(
    currentdate == null ? new Date() : new Date(`${currentdate}T10:20:30Z`)
  );
  //console.log("ddddd",currentdate)
  const [show, setShow] = useState(false);
  const [loading, setloading] = useState(true);
  const [list, setlist] = useState([]);
  const [see, setSee] = useState([]);
  const dimesions = useWindowDimensions();
  const navigations = useNavigation();
  const {dispatch} = useNavigation();
  const getdata = async () => {
    let reportDate = dateFormat(date, "yyyy-mm-dd");
    //console.log("report1", reportDate);
   // console.log("report2", data.user.DEPTCODE);
   // console.log("report3", data.user.ZONE_ID);
    setloading(true);
    setlist([]);
    axios({
      method: "POST",
      url: "https://us-central1-mn-npa-sop.cloudfunctions.net/getZarlanByDate",
      data: {
        reportDate,
        deptCode: data.user.DEPTCODE,
        zoneId: data.user.ZONE_ID,
      },
    })
      .then((res) => {
        setloading(false);
        setlist(res?.data?.result);
      })
      .catch((err) => {
        Alert.alert("Алдаа 1", err.toString());
        // console.log('err', err.message);
      });
  };

  useEffect(() => {
    let cleanup = getdata();

    return () => {
      cleanup;
    };
  }, [date]);

  return (
    <View style={{ flex: 1 }}>
      <Header title={"Зарлан мэдээлэл"} type={"menu"} />
      <Pressable
        style={[
          styles.button,
          { width: dimesions.width - 20, marginLeft: 10 },
          theme === "white"
            ? { borderColor: "#1c3587" }
            : { borderColor: "rgb(255,255,255,0.1)" },
        ]}
        onPress={() => dispatch(navigations.navigate("Date", { date, setDate }))}
      >
        <Text style={styles.textStyle}>{dateFormat(date, "yyyy-mm-dd")}</Text>
      </Pressable>

      {loading ? (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            alignSelf: "center",
          }}
        >
          <ActivityIndicator size="large" color="#1c3587" />
        </View>
      ) : list.length == 0 ? (
        <View
          style={{
            flex: 1,

            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            alignSelf: "center",
          }}
        >
          <Text
            style={{ fontSize: 20, color: "#000", justifyContent: "center" }}
          >
            Мэдээлэл байхгүй
          </Text>
        </View>
      ) : (
        <FlatList
          style={{ flex: 1, minHeight: dimesions.height - 150 }}
          data={list}
          renderItem={({ item, index }) => <Zarlan item={item} />}
          keyExtractor={(item, index) => item?.ID?.toString()}
        />
      )}
    </View>
  );
};

export default ZarlanScreen;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },

    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,

    padding: 10,
    elevation: 2,
    borderWidth: 1,
    marginTop: 10,
  },
  buttonClose: {
    backgroundColor: "#1c3587",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginTop: 10,
    width: 120,
  },
  textStyle: {
    color: "#000",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
