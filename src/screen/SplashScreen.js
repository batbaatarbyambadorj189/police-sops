import React, { useEffect, useState, useRef, useContext, Alert } from "react";
import { useNavigation } from "@react-navigation/native";
import {
  Image,
  Platform,
  ProgressBarAndroid,
  ProgressViewIOSBase,
  SafeAreaView,
  ActivityIndicator,
  StyleSheet,
  View,
  LogBox,
} from "react-native";
import * as Notifications from "expo-notifications";
import deviceStorage from "../store/deviceStorage";
import { DataContext } from "../store/DataContext";
import configs from "../config";

import axios from "axios";
import Constants from "expo-constants";

//check login user splash
const SplashScreen = () => {
  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState(false);
  const navigation = useNavigation();
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const notificationListener = useRef();
  const responseListener = useRef();
  axios.defaults.baseURL = configs.API_ROOT;

  useEffect(() => {
    setNoti();
  }, []);
  const setNoti = async () => {
    registerForPushNotificationsAsync()
      .then((token) => {
        setExpoPushToken(token);
      })
      .catch((err) => {
        setExpoPushToken(err.toString());
      });

    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        setNotification(notification);
      });

    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        navigation.navigate("ZarlanNoti", {
          item: response.notification.request.content.data,
        });
      });

    return () => {
      Notifications.removeNotificationSubscription(
        notificationListener.current
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  };
  const registerForPushNotificationsAsync = async () => {
    let token;

    const { status: existingStatus } =
      await Notifications.getPermissionsAsync();

    let finalStatus = existingStatus;
    if (existingStatus !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }

    if (finalStatus !== "granted") {
      Alert.alert("", t("action.ask.notification"), [
        {
          text: t("action.yes"),
          onPress: () => {
            Linking.openSettings();
          },
        },
      ]);
    }

    token = (await Notifications.getExpoPushTokenAsync()).data;

    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }

    console.log(
      "🚀 ~ file: Splash.js:80 ~ registerForPushNotificationsAsync ~ token",
      token
    );
    return token;
  };

  return (
    <View
      style={[
        styles.container,
        theme === "white"
          ? { backgroundColor: "#FFF" }
          : { backgroundColor: "#000" },
      ]}
    >
      <View style={styles.loader}>
        <Image style={styles.logo} source={require("../../assets/icon.png")} />
        <ActivityIndicator
          style={{ marginTop: 30 }}
          size="large"
          color="#1c3587"
        />
      </View>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Platform === "ios" ? 32 : 28,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffff",
  },
  loader: {
    flexDirection: "column",
  },
  logo: {
    height: 80,
    width: 80,
    resizeMode: "contain",
  },
  lottie: {
    width: 100,
    height: 100,
  },
});
