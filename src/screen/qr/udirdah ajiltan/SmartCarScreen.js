import React, { useEffect, useState, useContext } from "react";
import { StyleSheet, Text, View, Platform, TextInput, Button, Modal,TouchableOpacity, Image } from "react-native";
import Header from "../../component/header";
import { useNavigation } from "@react-navigation/native";
import { BarCodeScanner } from "expo-barcode-scanner";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Dimensions } from "react-native";
import axios from "axios";
import { DataContext } from "../../store/DataContext";
const SmartCarScreen = () => {
  const navigation = useNavigation();
  const windowWidth = Dimensions.get("window").width;
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const [hasPermission, setHasPermission] = useState(null);
  const [isScan, setIsScan] = useState(false)
  const [isProcessingScan, setIsProcessingScan] = useState(false);
  // const [number, setNumber] = useState("0233унч");
  const [number, setNumber] = useState("");
  const [scanned, setScanned] = useState(false);
  const [result, setResult] = useState(null);
  const [datas, setdatas] = useState(null);
  const [loading, setLoading] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [buttonText, setButtonText] = useState('Бүртгэх');
const [isButtonDisabled, setIsButtonDisabled] = useState(false);

const [resu, setResu] = useState(false);
  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    setdatas(data);

    // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };
  const getdata = async () => {
    
    console.log('dsdsdsdsd')
    try {
      
      console.log("datas",datas)//ХД-29292
      let  value  = datas;
      console.log("value",value)// is undefined 
// Use a regular expression to match the numeric part after "ХД-"
let regex = /ХД-(\d+)/;

let match = value.match(regex);


    // Extract the numeric part
    let privacyNumbers = match[1];
    
    console.log("res", privacyNumbers); // Output: "15340"

      console.log(
        "value",      
        data.user.USERNAMELOG,
        data.user.WORKHISTORY_ID,

      );
      await axios({
        method: "POST",
        url: "/police/zuvluguun2024",
        data: {
          privacyNumber: privacyNumbers,

        },
      })
        .then((res) => {
          console.log("====================================");
          console.log("res", res.data);
          setLoading(false);
          let result = res.data;
          setResult(result);
          console.log("====================================");
        })
        .catch((err) => {
          // console.log('====================================');
          console.log("err", err);
          // console.log('====================================');
        });
    } catch (err) {}
  };
  const handleInputChange = async(text) => {
    setNumber(text);
    ;
    // Check if the length of the input is 5, and trigger the search
    if (text.length === 5) {
      setModalVisible(true)
      // Perform your search or navigation logic here
      try {
        
        console.log(number)
  
      // Extract the numeric part
      let privacyNumbers = text;
      
      console.log("ressss", privacyNumbers); // Output: "15340"

        await axios({
          method: "POST",
          url: "/police/zuvluguun2024",
          data: {
            privacyNumber: privacyNumbers,
  
          },
        })
          .then((res) => {
            console.log("====================================");
            console.log("res", res.data);
            setLoading(false);
            let result = res.data;
            setResult(result);
            
            console.log("====================================");
          })
          .catch((err) => {
            // console.log('====================================');
            console.log("err", err);
            // console.log('====================================');
          });
      } catch (err) {}
    }
  };
  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);
  useEffect(() => {
    getdata(); 
    console.log("dddd",result)
    if (result === 'Мэдээлэл олдсонгүй!') {
      setButtonText('Бүртгэлгүй');
      setIsButtonDisabled(true);
    } else {
      setButtonText('Бүртгэх');
      setIsButtonDisabled(false);
    }// Call getdata when datas changes
  }, [datas]);
  if (hasPermission === null) {
    return (
      <View style={styles.container}>
        <Header title={"Зөвшөөрөл шалгах"} type={"menu"} />
        <View style={{ flex: 1, alignContent: "center", alignItems: "center" }}>
          <Text>Камерт хандах эрх олгоно уу.!!!</Text>
        </View>
      </View>
    );
  }
  if (hasPermission === false) {
    return (
      <View style={styles.container}>
        <Header title={"Зөвшөөрөл шалгах"} type={"menu"} />
        <View style={{ flex: 1, alignContent: "center", alignItems: "center" }}>
          <Text>Камерт хандах эрх олгоогүй байна.</Text>
        </View>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <Header title={"Зөвшөөрөл шалгах"} type={"menu"} />
      <View style={{ flex: 1, flexDirection: "column" }}>
        <View
          style={{
            flexDirection: "row",
            width: windowWidth,
            height: 50,
            backgroundColor: "#FFF",
          }}
        >
          <TextInput
            placeholder="Хувийн дугаар оруулах : 12345"
            maxLength={5}
            returnKeyType="next"
            value={number}
            onChangeText={(text) => handleInputChange(text)}
            style={{ flex: 1, padding: 3, paddingLeft: 20, width: windowWidth }}
          />
          
          <View style={[{ backgroundColor: "#1c3587", width: 50 },theme==="white"?{}:{backgroundColor:"rgba(0,0,0,0.8)",}]}>
            <MaterialCommunityIcons
              size={30}
              color="white"
              name="magnify"
              style={{ margin: 10 }}
              onPress={() => {
                navigation.navigate("QRDetail", {
                  value: number,
                  // resume: resume.bind(this),
                });
              }}
            />
          </View>
          
        </View>
        <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={closeModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTexts}>Ирц бүртгэх</Text>
            {result[0].PRIVACY_NO ? (
  <>
    <Image
      source={{
        uri: result[0].PIC_URL,
      }}
      style={[
        styles.profile,
        theme === "white"
          ? { borderWidth: 1, borderColor: "#000" }
          : { borderWidth: 1, borderColor: "#FFF" },
      ]}
      resizeMode="center"
    />
    <Text style={styles.modalText}>{result[0].WORKER_NAME}</Text>
  </>
) : (
  <Text>No data available</Text>
)}
              <View style={{flexDirection: "row",marginTop:30}}>
              {!isButtonDisabled && (
  <TouchableOpacity
    style={[styles.button, { backgroundColor: '#1c3587', marginTop: 10 }]}
    onPress={closeModal}
  >
    <Text style={{ color: 'white' }}>{buttonText}</Text>
  </TouchableOpacity>
)}
      <TouchableOpacity
        style={[styles.button, { backgroundColor: '#1c3587',marginLeft:60, marginTop: 10 }]}
        onPress={closeModal}
      >
        <Text style={{ color: 'white' }}>Хаах</Text>
      </TouchableOpacity>
      </View>
          </View>
        </View>
      </Modal>
        {scanned && <Button title={'Баркод уншуулах'} onPress={() => setScanned(false)} />}
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
          
          style={{
            flex: 1,
          }}
          
        ></BarCodeScanner>

        <View
          style={{
            width: "70%",
            height: "30%",
            alignSelf: "center",
            marginTop: "50%",
            borderWidth: 3,
            borderColor: "#fff",
            borderStyle: "dotted",
            zIndex: 10,
            borderRadius: 20,
            position: "absolute",
            flexDirection: "row",
          }}
        ></View>
      </View>
      
    </View>
  );
};

export default SmartCarScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor:'#1c3587',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
    width: 300, // Set the width as needed
    height: 390,
  },
  modalText: {
    fontSize: 20,
    marginBottom: 10,
  },
  modalTexts: {
    fontSize: 20,
    marginBottom: 10,
    fontWeight:'bold'
  },
  button: {
    backgroundColor: '#1c3587',
    padding: 10,
    borderRadius: 5,
    
  },
  profile: {
    width: 120,
    height: 150,
    borderRadius: 10,
  },
});
