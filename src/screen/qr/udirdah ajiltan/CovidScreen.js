import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Button,
  TouchableHighlight,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Header from "../../component/header";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import axios from "axios";
import { DataContext } from "../../store/DataContext";

const CovidScreen = (qr) => {
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const navigation = useNavigation();
  const [result, setResult] = useState(null);
  const [loading, setLoading] = useState(true);

  const getdata = async () => {
    
    try {
      let { params } = qr.route;
      let { value } = params;
      
// Use a regular expression to match the numeric part after "ХД-"
let regex = /ХД-(\d+)/;

let match = value.match(regex);


    // Extract the numeric part
    let privacyNumbers = match[1];
    
    console.log(result); // Output: "15340"

      console.log(
        "value",
        value,qr,
        
        data.user.USERNAMELOG,
        data.user.WORKHISTORY_ID,
        params,
      );
      await axios({
        method: "POST",
        url: "/police/zuvluguun2024",
        data: {
          privacyNumber: privacyNumbers,

        },
      })
        .then((res) => {
          console.log("====================================");
          console.log("res", res.data);
          setLoading(false);
          let result = res.data;
          setResult(result);
          console.log("====================================");
        })
        .catch((err) => {
          // console.log('====================================');
          console.log("err", err);
          // console.log('====================================');
        });
    } catch (err) {}
  };
  useEffect(() => {
    // const qrData =
    getdata();

    // console.log("CovidScreen - data", qrData);
    // return () => {
    //   qrData;
    // };
  }, []);

  return (
    <View style={[styles.container]}>
      <Header title={"Ковид вакцин"} type={"back"} />
      {loading ? (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            alignSelf: "center",
          }}
        >
          <ActivityIndicator size="large" color="#1c3587" />
        </View>
      ) : (
        result != null && (
          <View
            style={[
              {
                flex: 1,
                justifyContent: "center",

                alignItems: "center",

                flexDirection: "column",
              },
              theme === "white" ? {} : { backgroundColor: "#000" },
            ]}
          >
            <MaterialCommunityIcons
              name="qrcode"
              size={200}
              color={
                result && result.status == 0 && result.success == true
                  ? "green"
                  : "red"
              }
            />
            <Text
              style={[
                {
                  fontSize: 30,
                  color: "red",
                  justifyContent: "center",
                  fontWeight: "bold",
                  marginVertical: 15,
                },
                result && result?.status == 0 && result?.success == true
                  ? { color: "green" }
                  : { color: "red" },
              ]}
            >
              {result?.status == 0 && result?.success == true
                ? result?.message
                : result?.message}
            </Text>
            <TouchableHighlight
              onPress={() => {
                navigation.goBack();
              }}
              style={styles.buttonStyle}
            >
              <MaterialCommunityIcons
                style={{ paddingLeft: 22 }}
                name="arrow-left-thick"
                size={36}
                color="white"
              />
            </TouchableHighlight>
          </View>
        )
      )}
    </View>
  );
};

export default CovidScreen;

const styles = StyleSheet.create({
  buttonStyle: {
    width: 80,
    height: 80,
    backgroundColor: "#1c3587",
    justifyContent: "center",
    borderRadius: 80,
    alignSelf: "center",
    marginTop: 30,
    position: "absolute",
    bottom: 100,
  },
  container: {
    flex: 1,
    //backgroundColor:'#1c3587',
  },
  header: {
    fontSize: 18,
    fontStyle: "italic",
    marginTop: 5,
  },
  value: {
    fontSize: 18,
    fontWeight: "700",
  },
});
