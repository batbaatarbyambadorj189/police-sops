import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Button,
  TouchableHighlight,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Header from "../../component/header";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import axios from "axios";
import { DataContext } from "../../store/DataContext";
var dateFormat = require("dateformat");
const SmartCarDetailScreen = (qr) => {
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const navigation = useNavigation();
  const [result, setResult] = useState(null);
  const [loading, setLoading] = useState(true);

  const getdata = async () => {
    try {
      let { value } = qr.route.params;
      await axios({
        method: "POST",
        url: "/police/checkPermit",
        data: {
          type: value.length == 7 ? "number" : "qr",
          value,
          username: data.user.USERNAMELOG,
          workhistory: data.user.WORKHISTORY_ID,
        },
      })
        .then((res) => {
          // console.log('====================================');
          // console.log("res",res.data);
          setLoading(false);
          let result = res.data?.data;
          let tRange = "";
          if (result.entityPurposeType.allTimeRanges == false) {
            result.entityPurposeType.timeRanges.map((row) => {
              if (row.enabled == true) {
                // console.log('true',row.startHour + " - " + row.endHour + "(ЯВНА) \n");

                tRange += row.startHour + " - " + row.endHour + "(ЯВНА) \n";
              }
            });
            // console.log('tRange',tRange);

            result.entityPurposeType.timeRanges = tRange;
          }
          setResult(result);
          // console.log('====================================');
        })
        .catch((err) => {
          // console.log('====================================');
          // console.log("err",err);
          // console.log('====================================');
        });
    } catch (err) {}
  };
  useEffect(() => {
    getdata();
  }, []);

  return (
    <View style={[styles.container]}>
      <Header title={"Зөвшөөрөл"} type={"back"} />
      {loading ? (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            alignSelf: "center",
          }}
        >
          <ActivityIndicator size="large" color="#1c3587" />
        </View>
      ) : result == null ? (
        <View
          style={[
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
            },
            theme === "white" ? {} : { backgroundColor: "#000" },
          ]}
        >
          <Text
            style={{
              fontSize: 20,
              color: "red",
              justifyContent: "center",
              marginVertical: 15,
            }}
          >
            Зөвшөөрөлгүй тээврийн хэрэгсэл
          </Text>
          <TouchableHighlight
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.buttonStyle}
          >
            <MaterialCommunityIcons
              style={{ paddingLeft: 22 }}
              name="arrow-left-thick"
              size={36}
              color="white"
            />
          </TouchableHighlight>
        </View>
      ) : (
        <View
          style={[
            { flex: 1, flexDirection: "column", padding: 20 },
            theme === "white" ? {} : { backgroundColor: "#000" },
          ]}
        >
          <View style={{ flexDirection: "column" }}>
            <Text
              style={[
                styles.header,
                theme === "white" ? {} : { color: "#FFF" },
              ]}
            >
              Улсын дугаар
            </Text>
            <Text
              style={[styles.value, theme === "white" ? {} : { color: "#FFF" }]}
            >
              {result?.x}
            </Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Text
              style={[
                styles.header,
                theme === "white" ? {} : { color: "#FFF" },
              ]}
            >
              Марк
            </Text>
            <Text
              style={[styles.value, theme === "white" ? {} : { color: "#FFF" }]}
            >
              {result?.markName ? result?.markName : "Мэдээлэл байхгүй"}
            </Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <Text
              style={[
                styles.header,
                theme === "white" ? {} : { color: "#FFF" },
              ]}
            >
              Модел
            </Text>
            <Text
              style={[styles.value, theme === "white" ? {} : { color: "#FFF" }]}
            >
              {result?.modelName ? result?.modelName : "Мэдээлэл байхгүй"}
            </Text>
          </View>
          <View style={{ flexDirection: "column", paddingTop: 3 }}>
            <Text
              style={[
                styles.header,
                theme === "white" ? {} : { color: "#FFF" },
              ]}
            >
              Замын хөдөлгөөнд оролцох хугацаа
            </Text>
            <Text
              style={[styles.value, theme === "white" ? {} : { color: "#FFF" }]}
            >
              {result?.entityPurposeType?.allTimeRanges == "true" ||
              result?.entityPurposeType?.allTimeRanges == true
                ? "Цагийн хязгааргүй"
                : result?.entityPurposeType?.timeRanges}
            </Text>
          </View>
          <View style={{ flexDirection: "column", paddingTop: 3 }}>
            <Text
              style={[
                styles.header,
                theme === "white" ? {} : { color: "#FFF" },
              ]}
            >
              Зөршөөрөл эзэмшигч
            </Text>
            <Text
              style={[styles.value, theme === "white" ? {} : { color: "#FFF" }]}
            >
              {result?.displayName}
            </Text>
          </View>
          <View style={{ flexDirection: "column", paddingTop: 3 }}>
            <Text
              style={[
                styles.header,
                theme === "white" ? {} : { color: "#FFF" },
              ]}
            >
              Тусгай чиг үүрэг
            </Text>
            <Text
              style={[styles.value, theme === "white" ? {} : { color: "#FFF" }]}
            >
              {result?.name}
            </Text>
          </View>
          <View style={{ flexDirection: "column", paddingTop: 3 }}>
            <Text
              style={[
                styles.header,
                theme === "white" ? {} : { color: "#FFF" },
              ]}
            >
              Олгосон огноо
            </Text>
            <Text
              style={[styles.value, theme === "white" ? {} : { color: "#FFF" }]}
            >
              {dateFormat(result?.createdDate, "yyyy.MM.dd HH:MM:ss")}
            </Text>
          </View>
          {result?.seized && (
            <>
              <Text
                style={{
                  fontSize: 20,
                  color: "red",
                  justifyContent: "center",
                  marginVertical: 15,
                }}
              >
                Зөвшөөрөл цуцлагдсан
              </Text>
              <View style={{ flexDirection: "column", paddingTop: 3 }}>
                <Text
                  style={[
                    styles.header,
                    theme === "white" ? {} : { color: "#FFF" },
                  ]}
                >
                  Цуцалсан шалтгаан
                </Text>
                <Text
                  style={[
                    styles.value,
                    theme === "white" ? {} : { color: "#FFF" },
                  ]}
                >
                  {result?.seizeReason}
                </Text>
              </View>
              <View style={{ flexDirection: "column", paddingTop: 3 }}>
                <Text
                  style={[
                    styles.header,
                    theme === "white" ? {} : { color: "#FFF" },
                  ]}
                >
                  Тайлбар
                </Text>
                <Text
                  style={[
                    styles.value,
                    theme === "white" ? {} : { color: "#FFF" },
                  ]}
                >
                  {result?.seizeComment}
                </Text>
              </View>
            </>
          )}
        </View>
      )}
    </View>
  );
};

export default SmartCarDetailScreen;

const styles = StyleSheet.create({
  buttonStyle: {
    width: 80,
    height: 80,
    backgroundColor: "#1c3587",
    justifyContent: "center",
    borderRadius: 80,
    alignSelf: "center",
    marginTop: 30,
    position: "absolute",
    bottom: 100,
  },
  container: {
    flex: 1,
    //backgroundColor:'#1c3587',
  },
  header: {
    fontSize: 18,
    fontStyle: "italic",
    marginTop: 5,
  },
  value: {
    fontSize: 18,
    fontWeight: "700",
  },
});
