import React, { useEffect, useState, useContext } from "react";
import { StyleSheet, Text, View, Platform, TextInput } from "react-native";
import Header from "../../component/header";
import { useNavigation } from "@react-navigation/native";
import { BarCodeScanner } from "expo-barcode-scanner";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Dimensions } from "react-native";
import { DataContext } from "../../store/DataContext";
const SmartCarScreen = () => {
  const navigation = useNavigation();
  const windowWidth = Dimensions.get("window").width;
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const [hasPermission, setHasPermission] = useState(null);
  // const [number, setNumber] = useState("0233унч");
  const [number, setNumber] = useState("");
  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);
  const handleBarCodeScanned = ({ type, data }) => {
    // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    !isNaN(data)
      ? navigation.navigate("Covid", { value: data })
      : data.length > 24
      ? navigation.navigate("Covid", { value: data })
      : navigation.navigate("QRDetail", { value: data });
  };

  if (hasPermission === null) {
    return (
      <View style={styles.container}>
        <Header title={"Зөвшөөрөл шалгах"} type={"menu"} />
        <View style={{ flex: 1, alignContent: "center", alignItems: "center" }}>
          <Text>Камерт хандах эрх олгоно уу.!!!</Text>
        </View>
      </View>
    );
  }
  if (hasPermission === false) {
    return (
      <View style={styles.container}>
        <Header title={"Зөвшөөрөл шалгах"} type={"menu"} />
        <View style={{ flex: 1, alignContent: "center", alignItems: "center" }}>
          <Text>Камерт хандах эрх олгоогүй байна.</Text>
        </View>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <Header title={"Зөвшөөрөл шалгах"} type={"menu"} />
      <View style={{ flex: 1, flexDirection: "column" }}>
        {/* <View
          style={{
            flexDirection: "row",
            width: windowWidth,
            height: 50,
            backgroundColor: "#FFF",
          }}
        >
          <TextInput
            placeholder="Улсын дугаар оруулах : 0000УУУ"
            maxLength={7}
            returnKeyType="next"
            onChange={(text) => {
              setNumber(text.nativeEvent.text);
            }}
            style={{ flex: 1, padding: 3, paddingLeft: 20, with: windowWidth }}
          />
          <View style={[{ backgroundColor: "#1c3587", width: 50 },theme==="white"?{}:{backgroundColor:"rgba(0,0,0,0.8)",}]}>
            <MaterialCommunityIcons
              size={30}
              color="white"
              name="magnify"
              style={{ margin: 10 }}
              onPress={() => {
                navigation.navigate("QRDetail", {
                  value: number,
                  // resume: resume.bind(this),
                });
              }}
            />
          </View>
        </View> */}
        <BarCodeScanner
          onBarCodeScanned={handleBarCodeScanned}
          barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
          Bar
          style={{
            flex: 1,
          }}
        ></BarCodeScanner>

        <View
          style={{
            width: "70%",
            height: "30%",
            alignSelf: "center",
            marginTop: "50%",
            borderWidth: 3,
            borderColor: "#fff",
            borderStyle: "dotted",
            zIndex: 10,
            borderRadius: 20,
            position: "absolute",
            flexDirection: "row",
          }}
        ></View>
      </View>
    </View>
  );
};

export default SmartCarScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor:'#1c3587',
  },
});
