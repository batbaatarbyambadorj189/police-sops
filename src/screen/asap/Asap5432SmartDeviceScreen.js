
import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
  Platform,
  Button,TouchableOpacity,TextInput
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useIsFocused } from "@react-navigation/native";
import CodeList from "../../component/code/codeList";
import AsapItem from "../../component/asap/asapItem";
import AsapList from "../../component/asap/asapList";
import Header from "../../component/header";
import Pagination from "../../component/Pagination";
import { DataContext } from "../../store/DataContext";
import axios from "axios";
import SearchInBottomSheet from '../../component/SearchInBottomSheet';
import { SearchBar } from "@rneui/themed";
import Asap5432List from "../../component/asap/asap5400list";
import deviceStorage from "../../store/deviceStorage";
const Asap5432SmartDeviceScreen = () => {
  const BaseUrl ="http://www.rule.police.gov.mn"
  const dataContext = useContext(DataContext);
  const [data, setData] = dataContext;
  const [loading, setloading] = useState(true);
  const [list, setList] = useState([]);
  const navigations = useNavigation();
  const [currentPage, setCurrentPage] = useState(1);
  const [damjpage, setdamjPage] = useState(1);
  const [page, setPages] =useState(0);
  
  const [bottomSheetVisible, setBottomSheetVisible] = useState(false); // State to manage bottom sheet visibility
  const [query, setQuery] = useState('');
  const [filteredData, setFilteredData] = useState(data);
  const itemsPerPage = 10
  const [lists, setLists] = useState([]);
 const getData = async () => {
    try {
      setloading(true);
      let result = await axios({
        url: `/police/asap5432smartdevice`,
        method: "post",
        data: {

          username: "pu006025049",        
        },
      })
      //setharsan(result.data.isResult)
      //console.log("harsan",harsan)
      .then((res) => {
        //console.log("res code", res.data);
        console.log("res codes", res.data.totalRecords)
        setloading(false);
        //console.log("resdata",res.data)
        setList(res.data.data);
        setPages(res.data.pagination.pagecount)
        
        // const indexOfLastItem = currentPage * itemsPerPage;
        // console.log("indexoflast",indexOfLastItem)
        // const indexOfFirstItem = indexOfLastItem - itemsPerPage;
        // console.log("idexofFirst", list.slice(indexOfFirstItem, indexOfLastItem))
        //  setLists(list.slice(indexOfFirstItem, indexOfLastItem));
        // Change page
        const paginate = pageNumber => setCurrentPage(pageNumber);
      
      })
      .catch((err) => {
        setloading(false);
        //console.log("err code", err.message);
      });
  } catch (err) {
         if (err.response && err.response.status === 402) {
        deviceStorage.deleteStorage().then(() => {
      navigations.replace("Splash");
    });
      }
  }
  
  };
 // console.log("ddddddd",currentPage)


  useEffect(() => {
    getData();

  }, [itemsPerPage]);
  
  useEffect(() => {
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    setLists(list.slice(indexOfFirstItem, indexOfLastItem));
  }, [list, currentPage]);




 


  
  
  
  return (
    <View style={styles.container}>
    <Header title={"5432 Төхөөрөмж"} type={"menu"} /> 

    {loading ? (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          alignSelf: "center",
        }}
      >
        <ActivityIndicator size="large" color="#1c3587" />
      </View>
    ) : (
      <View style={{ flex: 1 }}>
        <FlatList
            data={lists}
            renderItem={({ item, index }) => (
              <Asap5432List item={item} />
            )}
            keyExtractor={(item, index) => item?.RN?.toString()}
          />
          {/* <Button title="Previous" onPress={goToPreviousPage} />
<Button title="Next" onPress={goToNextPage} /> */}
<View style={styles.paginationContainer}>
{page >0 &&<Pagination setCurrentPage={setCurrentPage} setPages={setPages} page={page} currentPage={currentPage} setdamjPage={setdamjPage}/> }

</View>
          {/* <Services/> */}
    
      </View>)
} 
  </View>
  )
}

export default Asap5432SmartDeviceScreen
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
  paginationContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom:10,
  },
  searchBarContainer: {
    backgroundColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
    marginHorizontal: 6,
       
    
  },
  searchBarInput: {
    backgroundColor: 'white',
  
  },
});
