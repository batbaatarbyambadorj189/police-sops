
import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
  Platform,
  Button,TouchableOpacity,TextInput
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Asap5400List from "../../component/asap/asap5400list";
import AsapList from "../../component/asap/asapList";
import Header from "../../component/header";
import Pagination from "../../component/Pagination";
import { DataContext } from "../../store/DataContext";
import axios from "axios";
import SearchInBottomSheet from '../../component/SearchInBottomSheet';
import { SearchBar } from "@rneui/themed";
import deviceStorage from "../../store/deviceStorage";
const Asap5428ZurchilScreen = () => {
  const BaseUrl ="http://www.rule.police.gov.mn"
  const dataContext = useContext(DataContext);
  const [data, setData] = dataContext;
  const [loading, setloading] = useState(true);
  const [list, setList] = useState([]);
  const navigations = useNavigation();
  const [currentPage, setCurrentPage] = useState(1);
  const [damjpage, setdamjPage] = useState(1);
  const [page, setPages] =useState(0);
  const [listAll, setListAll] = useState([]);
  const [bottomSheetVisible, setBottomSheetVisible] = useState(false); // State to manage bottom sheet visibility
  const [query, setQuery] = useState('');
  const [filteredData, setFilteredData] = useState(data);
  const itemsPerPage = 10
  const [lists, setLists] = useState([]);
  const [isChecked, setChecked] = useState(false);
  const toggleSwitch = () => setChecked(previousState => !previousState);
  const [bat, setBat] = useState(false);
  const updateBat = (previousState) => {
    // Update the bat state with new data
    setBat(previousState);
  };
  const getData = async () => {
    
    try {
      setloading(true);
     
      
      if (bat) {
        let result = await axios({
          url: `/police/asap5428zurchilall`,
          method: "post",
          data: {
            username: "pu006025049",
          },
          // headers: {
          //   Authorization: `Bearer ${token}`, // Include the token in the Authorization header
          //   'X-Secret-Key': secretKey, // Include the secret key in a custom header
          // },
        });

        setListAll(result.data.data);
        setPages(result.data.pagination.pagecount);
        
      } else {
        let result = await axios({
          url: `/police/asap5428zurchil`,
          method: "post",
          data: {
            username: "pu006025049",
          },
        });

        setList(result.data.data);
        setPages(result.data.pagination.pagecount);
      }

      setloading(false);
    } catch (err) {
      setloading(false);
            if (err.response && err.response.status === 402) {
        deviceStorage.deleteStorage().then(() => {
      navigations.replace("Splash");
    });
      }
    }
  };
 // console.log("ddddddd",currentPage)


  useEffect(() => {
    getData();

  }, [itemsPerPage, isChecked,bat]);
  

  useEffect(() => {
    const indexOfLastItem = currentPage * 10;
    const indexOfFirstItem = indexOfLastItem - 10;

    if (bat) {
      setLists(listAll.slice(indexOfFirstItem, indexOfLastItem));
    } else {
      setLists(list.slice(indexOfFirstItem, indexOfLastItem));
    }
  }, [isChecked, list, listAll, currentPage,bat]);




 


  
  
  
  return (
    <View style={styles.container}>
    <Header title={"5428 Зөрчил"} type={"menu"} category={"asap"} bat={bat} updateBat={updateBat}/> 

    {loading ? (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          alignSelf: "center",
        }}
      >
        <ActivityIndicator size="large" color="#1c3587" />
      </View>
    ) : (
      <View style={{ flex: 1 }}>
        <FlatList
            data={lists}
            renderItem={({ item, index }) => (
              <AsapList item={item} />
            )}
            keyExtractor={(item, index) => item?.RN?.toString()}
          />
          {/* <Button title="Previous" onPress={goToPreviousPage} />
<Button title="Next" onPress={goToNextPage} /> */}
<View style={styles.paginationContainer}>
{page >0 &&<Pagination setCurrentPage={setCurrentPage} setPages={setPages} page={page} currentPage={currentPage} setdamjPage={setdamjPage}/> }

</View>
          {/* <Services/> */}
    
      </View>)
} 
  </View>
  )
}

export default Asap5428ZurchilScreen
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
  paginationContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom:10,
  },
  searchBarContainer: {
    backgroundColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
    marginHorizontal: 6,
       
    
  },
  searchBarInput: {
    backgroundColor: 'white',
  
  },
});
