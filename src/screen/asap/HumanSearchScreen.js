
import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Text,
 View,
  ActivityIndicator,
  FlatList,
  Platform,
  Pressable,
  Button,TouchableOpacity,TextInput,Switch,RefreshControl, Modal, Alert,ScrollView, Image, SafeAreaView, Dimensions
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsapList from "../../component/asap/asapList";
import Header from "../../component/header";
import Pagination from "../../component/Pagination";
import { DataContext } from "../../store/DataContext";
import ImageViewer from "react-native-image-zoom-viewer";
import { MaterialCommunityIcons, AntDesign,FontAwesome,EvilIcons,Ionicons } from "@expo/vector-icons";
import axios from "axios";
import TopDropdown from "../../component/asap/TopDropdown";
import deviceStorage from "../../store/deviceStorage";
const HumanSearchScreen = (datas) => {
  const BaseUrl ="http://www.rule.police.gov.mn"
  const dataContext = useContext(DataContext);
  const [data, setData,theme, setTheme] = dataContext;
  const [searchInitiated, setSearchInitiated] = useState(false);
  const [loading, setloading] = useState(false);
  const [list, setList] = useState([]);
  const [listAll, setListAll] = useState([]);
  const navigation = useNavigation();
  const [currentPage, setCurrentPage] = useState(1);
  const [damjpage, setdamjPage] = useState(1);
  const [page, setPages] =useState(0);
  const [isChecked, setChecked] = useState(false);
  const itemsPerPage = 10
  const [lists, setLists] = useState([]);
  const [results, setResult] = useState();
  const [isEnabled, setIsEnabled] = useState(false);
  const [secretKey, setSecretKey] = useState(null);
  const [showImage, setshowImage] = useState(false);
  const [showImage5411, setshowImage5411] = useState(false);
  const [showImage5412, setshowImage5412] = useState(false);
  //const toggleSwitch = () => setChecked(previousState => !previousState);
  const [bat, setBat] = useState(false); // State for the bat data
  const [refreshing, setRefreshing] = useState(false); // Step 2: Define refreshing state variable
  const [modalVisible, setModalVisible] = useState(false);
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [image, setImage] = useState(false);
  // Function to handle refresh action
  const [searchQuery, setSearchQuery] = useState('');
  const [asap5411, setasap5411] = useState('');
  const [asap5412, setasap5412] = useState('');
  const [asapImage5411, setAsapImage5411] = useState('');
  const [asapImage5412, setAsapImage5412] = useState('');

const [selectedValue, setSelectedValue] = useState(null);
  const placeholderMap = {
    'Регистрийн дугаар': 'Регистрийн дугаараар',
    'Жолооны үнэмлэхний дугаар': 'Жолооны үнэмлэхний дугаараар',
    'Иргэний бүртгэлийн дугаар': 'Иргэний бүртгэлийн дугаараар',
  };
  const handleDropdownSelect = (option) => {
    setSelectedValue(option);
  };
  const handleUpdateData = () => {

    // if (datas?.updateBat) {
    //   datas.updateBat(isChecked); // Update bat data if updateBat function exists
    // }
    // if (datas?.updateFirstname) {
    //   datas.updateFirstname(firstname); // Update firstname if updateFirstname function exists
    // }
  
    setModalVisible(false); // Close the modal after updating data
  };
  const handleSearch = (text) => {
     if (text.length >= 5||text === '') {
      setSearchQuery(text); // Update state only if length meets minimum requirement
    }
    // You can perform search-related logic here, such as filtering data based on the search query
  };
  const searchs = () =>{
    setModalVisible(true);
  }
  const hideModal = () => {
    setModalVisible(false);
  };
  // Function to update bat data
//   const updateBat = (previousState) => {
//     // Update the bat state with new data
//     setBat(previousState);
//   };
//   const updateFirstname = (previousState) => {
//   setFirstname(previousState)
//   }
// const registerSearch = async () =>{

//   if(selectedValue == null){

// }
// else if(selectedValue == 'Регистрийн дугаар'){
// const fetchData = async () => {
//     try {
//       setloading(true);
//       let result = await axios({
//         url: `/police/driverLicenseSearch`,
//         method: "post",
//         data: {
//             REGISTER: searchQuery,
//             TOKEN: data?.token,
//         },
//       });
//       console.log("result", result.data.data);
//       //setasapsaw(result.data.l);
//       setResult(result.data)
//       //console.log(result.data)
//       console.log('res', results)
//       let result2  = await axios({
//         url: `/police/asap5411`,
//         method:"post",
//         data:{
//           register: searchQuery,
//           TOKEN: data?.token,
//         },
//       })
//         setasap5411(result2.data)
//       let result3  = await axios({
//         url: `/police/asap5412`,
//         method:"post",
//         data:{
//           register: searchQuery,
//           TOKEN: data?.token,
//         },
//       })
//       setasap5412(result3.data)
     
//       //setImage(results.Photo)
//       setloading(false);
//       if(result?.data?.status == "Driver not found"){
//         //console.log("wtf")
//         Alert.alert("Жолоочийн мэдээлэл олдсонгүй")
//         //setSearchInitiated(true)
//       }
//       console.log(result2.data)
//       console.log(result3.data)
//     } catch (err) {
//       setloading(false);
//       Alert.alert("Алдаа --", err.toString());
//     }
//   };
// }
// else if(selectedValue == 'Жолооны үнэмлэхний дугаар'){

// }
// else if(selectedValue == 'Иргэний бүртгэлийн дугаар'){
  
// }
// // if(searchQuery.length == 10){
// //    setSearchInitiated(true); 
// // fetchData()
// // }
// // else{
// //   Alert.alert("Регистрийн дугаараа шалгана уу")
// // }
   

// }

  const registerSearch = async () => {
    setSearchInitiated(true); 
    try {
      if (!selectedValue) {
        // Handle case when no option is selected
        return;
      }

      let endpoint1 = '';
      let endpoint2 = '';
      let endpoint3 = '';
      let requestData1 = {};
      let requestData2 = {};
      let requestData3 = {};
      let endpoint10 = '';
      let requestData10 = {};
      setloading(true)
      if (selectedValue === 'Регистрийн дугаар') {
        endpoint1 = `/police/driverLicenseSearch`;
        requestData1 = {
          REGISTER: searchQuery,
          TOKEN: data?.token,
        };
        endpoint2 = `/police/asap5411`;
        requestData2 = {
          register: searchQuery,
          TOKEN: data?.token,
        };
        endpoint3 = `/police/asap5412`;
        requestData3 = {
          register: searchQuery,
          TOKEN: data?.token,
        };
      }
      else if(selectedValue === 'Жолооны үнэмлэхний дугаар'){
           //console.log("sssssaaaa")
        endpoint1 = `/police/driverLicenseSearchLN`;
        requestData1 = {
          LICENSENUMBER: searchQuery,
          TOKEN: data?.token,
        };
        const response1 = await axios.post(endpoint1, requestData1);
        if (!response1.data) {
          console.error('No data in response1');
          return;
        }
        endpoint2 = `/police/asap5411`;
        requestData2 = {
          register: response1.data.RegistryNumber,
          TOKEN: data?.token,
        };
        endpoint3 = `/police/asap5412`;
        requestData3 = {
          register: response1.data.RegistryNumber,
          TOKEN: data?.token,
        };
      } 
      else if(selectedValue === 'Иргэний бүртгэлийн дугаар'){
        console.log("sssss")
        endpoint10 = `/police/getcitizenID`;
        requestData10 = {
          CIVILID: searchQuery,
          TOKEN: data?.token,
        };
       const response2 = await axios.post(endpoint10, requestData10);
       ///console.log(response2.data.response.regnum)
        if (response2?.data?.resultCode !== 0) {
          Alert.alert(response2?.data?.resultMessage)
          return;
        }
        let reg = response2?.data?.response?.regnum
        endpoint1 = `/police/driverLicenseSearch`;
        requestData1 = {
          REGISTER: response2?.data?.response?.regnum,
          TOKEN: data?.token,
        };
        endpoint2 = `/police/asap5411`;
        requestData2 = {
          register: response2?.data?.response?.regnum,
          TOKEN: data?.token,
        };
        endpoint3 = `/police/asap5412`;
        requestData3 = {
          register: response2?.data?.response?.regnum,
          TOKEN: data?.token,
        };
      }
      else {
        // Handle other selectedValue cases if needed
        setloading(false)
        return;
      }

      if (endpoint1 && endpoint2 && endpoint3) {
        //console.log("sksksksk")
        const [result1, result2, result3] = await Promise.all([
          axios.post(endpoint1, requestData1),
          axios.post(endpoint2, requestData2),
          axios.post(endpoint3, requestData3),
        ]);


       if (result1 && result2 && result3) {
        setResult(result1.data);
        setasap5411(result2.data);
        setasap5412(result3.data);
        setloading(false)
      } else {
        console.log('Invalid selected value:', selectedValue);
         setloading(false)
      }
    }
    } catch (error) {
         if (error.response && error.response.status === 402) {
      // Handle 402 Payment Required error
      Alert.alert('Нэвтрэлтийн хугацаа дууссан байна.');
       deviceStorage.deleteStorage().then(() => {
      navigation.replace("Splash");
    });
    } else {
      Alert.alert('Алдаа', error.toString());
    }
    setloading(false);
    }
  };
  const onRefresh = async () => {
    setRefreshing(true); 
    setPages(1);
    setBat(false)
    setFirstname("")// Set refreshing to true to show the spinner
    await fetchData(); // Call getData function to fetch data
    setRefreshing(false); // Set refreshing to false when done refreshing
  };
//   const getData = async () => {
    
//     try {
//       setloading(true);
      
//       if (firstname !== null && firstname !== '') 
//       {
//         console.log("hooson bish")
//         let result = await axios({
//           url: `/police/asap5411abhall`,
//           method: "post",
//           data: {
//             username: "pu041092104",
//           },
//         });

//         setList(result?.data?.data);
//         setPages(result?.data?.pagination?.pagecount)
//     }
//     else{
//       console.log("hooson")
//       if (bat) {
//         let result = await axios({
//           url: `/police/asap5411abhall`,
//           method: "post",
//           data: {
//             username: "pu041092104",
//           },
//           // headers: {
//           //   Authorization: `Bearer ${token}`, // Include the token in the Authorization header
//           //   'X-Secret-Key': secretKey, // Include the secret key in a custom header
//           // },
//         });

//         setListAll(result?.data?.data);
//         setPages(result?.data?.pagination?.pagecount);
        
//       } else {
//         let result = await axios({
//           url: `/police/asap5411abh`,
//           method: "post",
//           data: {
//             username: "pu041092104",
//           },
//         });

//         setList(result?.data?.data);
//         setPages(result?.data?.pagination?.pagecount);
//       }
//     }
//       setloading(false);
//     } catch (err) {
//       setloading(false);
//       console.error("Error:", err.message);
//     }
//   };



//   useEffect(() => {
//     //fetchData(); 
//     //setChecked(bat); 
//     //setImage(result.Photo)
//   }, [isChecked, itemsPerPage, bat,firstname]);


//   useEffect(() => {
//     const indexOfLastItem = currentPage * 10;
//     const indexOfFirstItem = indexOfLastItem - 10;

//     if (bat) {
//       setLists(listAll.slice(indexOfFirstItem, indexOfLastItem));
//     } else {
//       setLists(list.slice(indexOfFirstItem, indexOfLastItem));
//     }
//   }, [isChecked, list, listAll, currentPage,bat,firstname]);
  //console.log("bat.....",results)
  useEffect(() => {
    //fetchData();
    
    setImage(results?.BLOB)

  }, [results,asap5411,asap5412]);
  //("firstname--",asap5411)
const screenWidth = Dimensions.get('window').width;
// const createdate = new Date(asap5411.CREATEDDATE);
  //const formattedDates = createdate.toISOString().split("T")[0];
    
   const renderItem = ({ item, BNO_ID }) => {
    if(asap5411[0].BNO_ID !== BNO_ID){
    const date = new Date(item?.CREATEDDATE);
    //console.log("date",date)
const formattedDate = date.toISOString().split('T')[0];
    return (
    <View>
      <Modal visible={showImage5411} style={{ width: "100%", height: "100%" }}>
        <ImageViewer imageUrls={[{ url: item?.MPICURL }]} />
        <MaterialCommunityIcons
          color={"#fff"}
          size={30}
          name="close"
          style={{
            position: "absolute",
            right: 20,
            top: 50,

            width: 30,
            height: 30,
          }}
          onPress={() => {
            setshowImage5411(false);
          }}
        />
      </Modal>
    
        <View style={styles.content}>
          
    <View style={styles.row}>
      <View style={styles.column}>
       {asap5411 == "" ? (
            <ActivityIndicator
              color="#1c3587"
              size="large"
              style={{ marginTop: 30 }}
            />
          ) : (
            item?.MPICURL  != false && (
               
              <TouchableOpacity
                onPress={() => {
                  setshowImage5411(true);
                }}
              >
                <Image
                  style={styles.logo}
                  resizeMode="contain"
                  source={{
                  uri: item?.MPICURL,
                }}
                />
                
              </TouchableOpacity>
           
            )
          )}
     <Text style={[{ color: 'blue', marginBottom: 5 },theme === "white" ? {} : { color: "#FFF" },]}>{item?.REGISTER?.toUpperCase()}</Text>

     
     </View>
      <View style={{ marginLeft: 5, flex: 1 }}>
        <View style={styles.row}>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {item?.MTYPE + " "}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>ЭС зарлалтын №: {item?.M5400BNO}</Text>
            <Text style={styles.title}>Маягтын №: {item?.MBNO}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Хариуцсан алба хаагч</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.GAZARNAME + ", " + item?.WORKER + " "}
            </Text>
          </View>
          <View style={styles.row}>
            <Text style={{ fontSize: 15 }}>
              {"Шивсэн огноо: "  + formattedDate+ " "}
            </Text>
          </View>
        <Text style={styles.title}>Алга болсон огноо</Text>
            <Text style={{ textAlign: "justify" }}>
              {/* {formattedDate1 + " - " + formattedDate2} */}
              {item?.MDATE}
            </Text>
        <Text style={[{ color: 'blue', marginBottom: 5 },theme === "white" ? {} : { color: "#FFF" }]}>{item?.MINFO}</Text>
        <Text style={[{ textAlign: "justify" },theme === "white" ? {} : { color: "#FFF" }]} numberOfLines={4}>{item?.M5400INFO}</Text>

        
      </View>
    </View>
    
    <View style={styles.minicon}>
            <Text style={styles.title}>Анхаарах зүйлс</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.M5400ANHAARAH + " "}
            </Text>
          </View>
  </View>
  </View>)
    }
    else {
      return(
        <Text> Мэдээлэл олдсонгүй</Text>
      )
    }
};
     const renderItem5412 = ({ item, BNO_ID }) => {
    if(asap5412[0].BNO_ID !== BNO_ID){
    const date = new Date(item?.CREATEDDATE);
    //console.log("date",date)
const formattedDate = date.toISOString().split('T')[0];
    return (
    <View>
      <Modal visible={showImage5412} style={{ width: "100%", height: "100%" }}>
        <ImageViewer imageUrls={[{ url: item?.MPICURL }]} />
        <MaterialCommunityIcons
          color={"#fff"}
          size={30}
          name="close"
          style={{
            position: "absolute",
            right: 20,
            top: 50,

            width: 30,
            height: 30,
          }}
          onPress={() => {
            setshowImage5411(false);
          }}
        />
      </Modal>
    
        <View style={styles.content}>
          
    <View style={styles.row}>
      <View style={styles.column}>
       {asap5411 == "" ? (
            <ActivityIndicator
              color="#1c3587"
              size="large"
              style={{ marginTop: 30 }}
            />
          ) : (
            item?.MPICURL  != false && (
               
              <TouchableOpacity
                onPress={() => {
                  setshowImage5411(true);
                }}
              >
                <Image
                  style={styles.logo}
                  resizeMode="contain"
                  source={{
                  uri: item?.MPICURL,
                }}
                />
                
              </TouchableOpacity>
           
            )
          )}
     <Text style={[{ color: 'blue', marginBottom: 5 },theme === "white" ? {} : { color: "#FFF" },]}>{item?.REGISTER?.toUpperCase()}</Text>

     
     </View>
      <View style={{ marginLeft: 5, flex: 1 }}>
        <View style={styles.row}>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {item?.MTYPE + " "}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>ЭС зарлалтын №: {item?.M5400BNO}</Text>
            <Text style={styles.title}>Маягтын №: {item?.MBNO}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Хариуцсан алба хаагч</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.GAZARNAME + ", " + item?.WORKER + " "}
            </Text>
          </View>
          <View style={styles.row}>
            <Text style={{ fontSize: 15 }}>
              {"Шивсэн огноо: "  + formattedDate+ " "}
            </Text>
          </View>
        <Text style={styles.title}>Алга болсон огноо</Text>
            <Text style={{ textAlign: "justify" }}>
              {/* {formattedDate1 + " - " + formattedDate2} */}
              {item?.MDATE}
            </Text>
        <Text style={[{ color: 'blue', marginBottom: 5 },theme === "white" ? {} : { color: "#FFF" }]}>{item?.MINFO}</Text>
        <Text style={[{ textAlign: "justify" },theme === "white" ? {} : { color: "#FFF" }]} numberOfLines={4}>{item?.M5400INFO}</Text>

        
      </View>
    </View>
    
    <View style={styles.minicon}>
            <Text style={styles.title}>Анхаарах зүйлс</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.M5400ANHAARAH + " "}
            </Text>
          </View>
  </View>
  </View>)
    }
    else {
      return(
        <Text> Мэдээлэл олдсонгүй</Text>
      )
    }
};  
console.log('skjksd',results)
  return (
    <View style={styles.container}>
<View style={styles.container}>
      <Header title={"Хайлт"} type={"menu"}/>
      <TopDropdown
        options={['Регистрийн дугаар', 'Жолооны үнэмлэхний дугаар', 'Иргэний бүртгэлийн дугаар']}
        onSelect={handleDropdownSelect}
      />
      <View style={styles.cardsContainer}>
         
                  <View style={styles.cardContainerGlue}>

                    <Ionicons
                      name="search"
                      size={35}
                      style={{ marginLeft: 1 }}
                      color="rgba(0,0,0,1)"
                    />
     {/* <TouchableOpacity onPress={searchs}> */}
  <TextInput
    placeholder={placeholderMap[selectedValue] || 'Хайх төрлөө сонгоно уу'}
    onChangeText={setSearchQuery}
    name="Register"                
    value={searchQuery}
    style={[styles._textInputStyle, { flex: 1 }]}
    onSubmitEditing={registerSearch}
    maxLength={12}
  />

  <TouchableOpacity
      style={[styles.loginButtonStyle, theme !== "white" && { backgroundColor: "rgba(0,0,0,0.7)" }]}
      onPress={() => {
        registerSearch();
      }}
    >
      <Text style={
        styles.loginButtonTextStyle
       
      }>Хайх</Text>
    </TouchableOpacity>

{/* </TouchableOpacity> */}
                      </View>
                      </View>
                      <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={hideModal}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
              <View style={styles.section}>
        </View>
        <View style={styles.cardContainer}>
        <View style={styles.cardTextContainer}>
                      <Text style={styles._textStyle}>Овог</Text>
                      <TextInput
                        placeholder="Эцэг/эхийн нэр"
                        keyboardType="default"
                        name="privacyNo"
                        maxLength={32}
                        returnKeyType={Platform.OS === "ios" ? "done" : "next"}
                        value={lastname}
                        onChange={(text) => {
                          setLastname(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                    </View>
                    <View style={styles.cardContainer}>
        <View
                      style={(styles.cardTextContainer, { marginLeft: 10 })}
                    >
                      <Text style={styles._textStyle}>Нэр</Text>
                      <TextInput
                        placeholder="Нэр"
                        maxLength={32}
                        keyboardType="default"
                        value={firstname}
                        returnKeyType={Platform.OS === "ios" ? "done" : "next"}
                        onChange={(text) => {
                          setFirstname(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                    </View>
                {/* <Text style={styles.modalText}>Бүх мэдээллийг харуулах</Text> */}
                <View style={styles.section}>
                <Pressable style={styles.buttonClose} onPress={handleUpdateData}>
                  <Text style={styles.textStyle}>Хайлт хийх</Text>
                </Pressable>
                <Pressable style={styles.buttonClose} onPress={hideModal}>
                  <Text style={styles.textStyle}>Хаах</Text>
                </Pressable>
                </View>
              </View>
            </View>
          </Modal>
          {!searchInitiated ? (
        // Display "хоосон" (empty) if search is not initiated
        <Text>

        </Text>
      ) :
      loading ? (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          alignSelf: "center",
        }}
      >
        <ActivityIndicator size="large" color="#1c3587" />
      </View>
    ) : (

      <View style={{ flex: 1, backgroundColor:"white" }}>
              {/* <View style={styles.rowSpace}>
                  <Text
                    style={[
                      styles.textStyle,
                      theme === "white" ? { color: "#000" } : { color: "#FFF" },
                    ]}
                  >
                    Бүх мэдээлллийг харуулах{" "}
                  </Text>
                  <Switch
        
        thumbColor={isChecked ? '#1c3587' : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isChecked}
        style={styles.switcher}
        trackColor={{ false: "#000", true: "#fff" }}
        
      />
                </View> */}
                
      {/* <FlatList
            data={lists}
            renderItem={({ item, index }) => (
              <AsapList item={item} />
            )}
            keyExtractor={(item, index) => item?.RN?.toString()}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh} // Step 4: Implement onRefresh function
              />
            }
          />

<View style={styles.paginationContainer}>
{page >0 &&<Pagination setCurrentPage={setCurrentPage} setPages={setPages} page={page} currentPage={currentPage} setdamjPage={setdamjPage}/> }

</View> */}

<Modal visible={showImage} style={{ width: "100%", height: "100%" }}>
        <ImageViewer imageUrls={[{ url: `data:image/jpg;base64,${image}` }]} />
        <MaterialCommunityIcons
          color={"#fff"}
          size={30}
          name="close"
          style={{
            position: "absolute",
            right: 20,
            top: 50,

            width: 30,
            height: 30,
          }}
          onPress={() => {
            setshowImage(false);
          }}
        />
      </Modal>

    <View style={styles.safearea} >
      <View style={{  height:50, borderRadius:8}}>
      <Text style={{fontSize:22, fontWeight: 'bold'}}>Жолооны үнэмлэхний мэдээлэл</Text>
      </View>
{results?.status === 'Driver not found' ? ( 
  <View style={{backgroundColor: 'transparent'}}> 
     <Text style={{textAlign:'center', backgroundColor:'white', fontSize:17, padding:20}}>Мэдээлэл олдсонгүй</Text>
  </View>
 
):(<View> 
      { screenWidth <= 400 ? (
<View style={styles.row}>
      <View style={styles.column}>
        {/* <View style={{ padding: 12, flexDirection: "column", flex: 1 }}> */}
         
          {results == "" ? (
            <ActivityIndicator
              color="#1c3587"
              size="large"
              style={{ marginTop: 30 }}
            />
          ) : (
            image != false && (
               
              <TouchableOpacity
                onPress={() => {
                  setshowImage(true);
                }}
              >
                <Image
                  style={styles.logo}
                  resizeMode="contain"
                  source={{
                  uri: `data:image/jpg;base64,${image}`,
                }}
                />
                
              </TouchableOpacity>
           
            )
          )}
          
          <View style={styles.minicon}>
            <Text style={styles.title}>Регистрийн дугаар:</Text>
            <Text style={{ textAlign: "justify" }}>
              {results?.RegistryNumber}
            </Text>
          </View>
          <View style={styles.minicon}>
          <Text style={styles.title}>Эцэг/эхийн нэр:</Text>
            <Text style={{ fontSize: 15, }}>
              { results?.ParentNameL}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Нэр:</Text>
            <Text style={{ textAlign: "justify" }}>
              {/* {formattedDate1 + " - " + formattedDate2} */}
              {results?.DescriptionL}
            </Text>
            </View>
          {/* </View> */}
          <View style={styles.minicon}>
            <Text style={styles.title}>Хүйс:</Text>
            <Text style={{ textAlign: "justify" }}>
              {/* { item?.LASTNAME?.toUpperCase() + " -н " + item?.FIRSTNAME?.toUpperCase() + " - "+ item?.SEX + ", " + item?.AGE + " настай"}
               */}   
               {results?.Gender != "F" ? "Эрэгтэй" : "Эмэгтэй"
}
            </Text>
            </View>

            <View style={styles.minicon}>
            <Text style={styles.title}>Утасны дугаар:</Text>
            <Text style={styles.text}>{results?.CellPhone1}</Text>
          </View>
          
          </View>
          <View style={styles.column2}>
          <View>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {"Төлөв: " + results?.LicenseStatus}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Үнэмлэхний дугаар:</Text>
            <Text style={styles.text}> {results?.LicenseNumber}</Text>
            <Text style={styles.title}>Ангилал:</Text>
            <Text style={styles.text}> {results?.Classification}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Цусны бүлэг:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.BloodType}</Text>
          </View>

          <View style={styles.minicon}>
            <Text style={styles.title}>Төрсөн огноо:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.BirthDate}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Жолооч болсон огноо:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.DriverDate}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Үнэмлэх олгогдсон огноо:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.LicenseDate}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Хүчинтэй хугацаа:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.ExpireDate}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Гэрийн хаяг:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.Address}</Text>
          </View>
          
         </View>
         
        </View>
             ): (
              <View style={styles.row}>
      <View style={styles.column}>
        {/* <View style={{ padding: 12, flexDirection: "column", flex: 1 }}> */}
         
          {results == " " ? (
            <ActivityIndicator
              color="#1c3587"
              size="large"
              style={{ marginTop: 30 }}
            />
          ) : (
            image != false && (
               
              <TouchableOpacity
                onPress={() => {
                  setshowImage(true);
                }}
              >
                <Image
                  style={styles.logo}
                  resizeMode="contain"
                  source={{
                  uri: `data:image/jpg;base64,${image}`,
                }}
                />
                
              </TouchableOpacity>
           
            )
          )}
          
          <View style={styles.minicon}>
            <Text style={styles.title}>Регистрийн дугаар:</Text>
            <Text style={{ textAlign: "justify" }}>
              {results?.RegistryNumber}
            </Text>
          </View>
          
          {/* </View> */}
          <View style={styles.minicon}>
            <Text style={styles.title}>Хүйс:</Text>
            <Text style={{ textAlign: "justify" }}>
              {/* { item?.LASTNAME?.toUpperCase() + " -н " + item?.FIRSTNAME?.toUpperCase() + " - "+ item?.SEX + ", " + item?.AGE + " настай"}
               */}   
               {results?.Gender != "F" ? "Эрэгтэй" : "Эмэгтэй"
}
            </Text>
            </View>

          
          
          </View>
          <View style={styles.column2}>
          <View>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {"Төлөв: " + results?.LicenseStatus}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Үнэмлэхний дугаар:</Text>
            <Text style={styles.text}> {results?.LicenseNumber}</Text>
            <Text style={styles.title}>Ангилал:</Text>
            <Text style={styles.text}> {results?.Classification}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Цусны бүлэг:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.BloodType}</Text>
          </View>
<View style={styles.minicon}>
          <Text style={styles.title}>Эцэг/эхийн нэр:</Text>
            <Text style={{ fontSize: 15, }}>
              { results?.ParentNameL}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Нэр:</Text>
            <Text style={{ textAlign: "justify" }}>
              {/* {formattedDate1 + " - " + formattedDate2} */}
              {results?.DescriptionL}
            </Text>
            </View>
          
         </View>
         <View style={styles.column2}>
          <View style={styles.minicon}>
            <Text style={styles.title}>Төрсөн огноо:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.BirthDate}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Жолооч болсон огноо:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.DriverDate}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Үнэмлэх олгогдсон огноо:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.LicenseDate}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Хүчинтэй хугацаа:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.ExpireDate}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Гэрийн хаяг:</Text>
            <Text style={{ textAlign: "justify" }}>{results?.Address}</Text>
          </View>
            <View style={styles.minicon}>
            <Text style={styles.title}>Утасны дугаар:</Text>
            <Text style={styles.text}>{results?.CellPhone1}</Text>
          </View>
         </View>
        </View>
             )}
             </View>
             )}
        <View style={styles.temdeglel}>
            <Text style={styles.title}>Тэмдэглэл:</Text>
            <Text style={styles.text}>{results?.ActionNote}</Text>
          </View>

      
          </View>

     <View style={[{marginTop:16,paddingLeft:16, borderTopWidth: 3, borderColor: 'white',  height:50, backgroundColor:"#E0e2e2",
    }, asap5411 !=='Мэдээлэл олдсонгүй!' ? {borderTopEndRadius:16, borderTopStartRadius:16} : {borderRadius:16}]}>
      <Text style={{fontSize:22, fontWeight: 'bold'}}>Эрэн сурвалжлалтын мэдээлэл/5411/</Text>
      </View>

      {asap5411 && asap5411 !=='Мэдээлэл олдсонгүй!' ? (
        <FlatList
          data={asap5411}
          renderItem={renderItem}
          keyExtractor={(item) => item.RN}
          style={{ flex: 1, backgroundColor: '#E0e2e2', borderEndStartRadius: 16 , borderEndEndRadius:16 }}
        />
      ) : (
        <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 18 }}>мэдээлэл олдсонгүй</Text>
        </View>
      )}

<View style={[{marginTop:16,paddingLeft:16, borderTopWidth: 3, borderColor: 'white',  height:50,backgroundColor:"#E0e2e2"},asap5412 !=='Мэдээлэл олдсонгүй!' ? {borderTopEndRadius:16, borderTopStartRadius:16} : {borderRadius:16}]}>
      <Text style={{fontSize:22, fontWeight: 'bold'}}>Эрэн сурвалжлалтын мэдээлэл/5412/</Text>
      </View>
           {asap5412 && asap5412 !=='Мэдээлэл олдсонгүй!' ? (
        <FlatList
          data={asap5412}
          renderItem={renderItem5412}
          keyExtractor={(item) => item.RN}
          style={{ flex: 1, backgroundColor: '#E0e2e2', borderEndStartRadius: 16 , borderEndEndRadius:16}}
        />
      ) : (
        <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 18 }}>мэдээлэл олдсонгүй</Text>
        </View>
      )}
</View>
    )}
    </View>
</View>
  )
}

export default HumanSearchScreen
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
  content:{
    paddingLeft:18,
    paddingRight:20
  },
  paginationContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom:10,
  },
  searchBarContainer: {
    backgroundColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
    marginHorizontal: 6,
       
    
  },
  textStyle: {
    paddingTop: 17,
    fontSize: 17,
    fontWeight: "500",
  },
  searchBarInput: {
    backgroundColor: 'white',
  
  },
  rowSpace: {
        marginHorizontal: 25,
        marginVertical: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    // borderWidth:1,
    // borderRadius:16,
    // width:300,
    // marginBottom:10,
    // paddingBottom:20
    // width: ScreenWidth - 80,
  },
  section: {
    marginHorizontal: 10,
    marginVertical: 4,
    flexDirection: 'row',
    alignItems: 'center',
    padding:5,
  },
  paragraph: {
    fontSize: 20,
  },
  switcher: {
    marginTop: 20,
    marginRight: 10,
  },
  checkbox: {
    margin: 8,
    borderRadius:8,
    width: 30,  // Set your desired width
  height: 30,
    
  },
  _textInputStyle: {
    fontSize: 17,
    color: "#000",
    fontWeight: "900",
    width: "100%",
    minWidth: "70%",
    
  },
  cardContainerGlue: {
    // flex: 1,
    marginLeft: 24,
    marginRight: 24,
    alignItems: "center",
    flexDirection: "row",
  },
  cardTextContainer: {
    flex: 1,
    marginLeft: 12,
    flexDirection: "column",
    justifyContent: "center",
    marginTop: Platform === "ios" ? null : 10,
  },
  cardsContainer: {
    //margin: 8,
    height: 55,
    width: "100%",
    marginTop: 0,
    //borderRadius: 24,
    borderBottomEndRadius:24,
    borderBottomStartRadius:24,
    justifyContent: "center",
    backgroundColor: "white",
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Adjust the opacity as needed
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '70%',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  _InputStyle: {
    fontSize: 14,
    color: "#000",
    fontWeight: "800",
  },
  _textStyle: {
    fontSize: 16,
    fontWeight: "700",
    color: "#000",
    // backgroundColor:'yellow',
  },
  buttonClose: {
    backgroundColor: '#1c3587',
    borderRadius:16,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    padding:8,
    //margin:8,
    paddingRight:12,
    paddingLeft:12
  },
  cardContainer: {
    margin: 8,
    height: 55,
    width: "100%",
    marginTop: 0,
    borderRadius: 20,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.1)",
  },
  title:{
    color: "gray",
    fontSize: 13,
  },
  text:{
    fontWeight:"400",
    fontSize:15,
    textAlign:"justify"
  },
  logo:{
    width:130,
    height:165,
    borderRadius:16,
  },
  row: {
    //paddingTop: 10,
    flexDirection: "row",
    //justifyContent: "space-between",
    //backgroundColor:"red"
  },
    row2: {
    paddingTop: 12,
    flexDirection: "row",
    //justifyContent: "space-between",
    //backgroundColor:"red"
  },
  column: {
    //paddingTop: 10,
    flexDirection: "column",
    //justifyContent: "space-between",
    padding:12,
  },
  column2: {
    //paddingTop: 10,
    flex:1,
    flexDirection: "column",
    //justifyContent: "space-between",
    padding:4,
    paddingRight:10,
    //alignItems:'flex-start'
  },
  temdeglel:{
    paddingLeft:12,
    paddingRight:12,
    flex:1,
  },
  loginButtonStyle: {
    borderRadius: 16,
  paddingVertical: 10,
  paddingHorizontal: 20,
  backgroundColor: '#1c3587',
  position: 'absolute', // Position the button absolutely
  right: 0,
  
  },
  loginButtonTextStyle: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
    //marginRight: 15,
  },
  safearea:{
    backgroundColor:"#E0e2e2",
    borderBottomRightRadius:16,
    borderBottomLeftRadius:24,
    paddingLeft:16,
    paddingRight:4,
    paddingTop:12
  }
});
