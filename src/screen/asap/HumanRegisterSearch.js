
import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
  Platform,
  Pressable,
  Button,TouchableOpacity,TextInput,Switch,RefreshControl, Modal
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsapList from "../../component/asap/asapList";
import Header from "../../component/header";
import Pagination from "../../component/Pagination";
import { DataContext } from "../../store/DataContext";
import { MaterialCommunityIcons, AntDesign,FontAwesome,EvilIcons,Ionicons } from "@expo/vector-icons";
import axios from "axios";
import { SearchBar } from '@rneui/themed';
import config from "../../config";
import Checkbox from 'expo-checkbox';
const HumanSearchScreen = (datas) => {
  const BaseUrl ="http://www.rule.police.gov.mn"
  const dataContext = useContext(DataContext);
  const [data, setData,theme, setTheme] = dataContext;
  const [loading, setloading] = useState(true);
  const [list, setList] = useState([]);
  const [listAll, setListAll] = useState([]);
  const navigations = useNavigation();
  const [currentPage, setCurrentPage] = useState(1);
  const [damjpage, setdamjPage] = useState(1);
  const [page, setPages] =useState(0);
  const [isChecked, setChecked] = useState(false);
  const itemsPerPage = 10
  const [lists, setLists] = useState([]);
  const [isEnabled, setIsEnabled] = useState(false);
  const [secretKey, setSecretKey] = useState(null);
  //const toggleSwitch = () => setChecked(previousState => !previousState);
  const [bat, setBat] = useState(false); // State for the bat data
  const [refreshing, setRefreshing] = useState(false); // Step 2: Define refreshing state variable
  const [modalVisible, setModalVisible] = useState(false);
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  // Function to handle refresh action
  const [searchQuery, setSearchQuery] = useState('');
  const handleUpdateData = () => {

    // if (datas?.updateBat) {
    //   datas.updateBat(isChecked); // Update bat data if updateBat function exists
    // }
    // if (datas?.updateFirstname) {
    //   datas.updateFirstname(firstname); // Update firstname if updateFirstname function exists
    // }
  
    setModalVisible(false); // Close the modal after updating data
  };
  const handleSearch = (text) => {
    setSearchQuery(text);
    // You can perform search-related logic here, such as filtering data based on the search query
  };
  const searchs = () =>{
    setModalVisible(true);
  }
  const hideModal = () => {
    setModalVisible(false);
  };
  // Function to update bat data
//   const updateBat = (previousState) => {
//     // Update the bat state with new data
//     setBat(previousState);
//   };
//   const updateFirstname = (previousState) => {
//   setFirstname(previousState)
//   }
  const onRefresh = async () => {
    setRefreshing(true); 
    setPages(1);
    setBat(false)
    setFirstname("")// Set refreshing to true to show the spinner
    await getData(); // Call getData function to fetch data
    setRefreshing(false); // Set refreshing to false when done refreshing
  };
  const getData = async () => {
    
    try {
      setloading(true);
      
      if (firstname !== null && firstname !== '') 
      {
        console.log("hooson bish")
        let result = await axios({
          url: `/police/asap5411abhall`,
          method: "post",
          data: {
            username: "pu041092104",
          },
        });

        setList(result?.data?.data);
        setPages(result?.data?.pagination?.pagecount)
    }
    else{
      console.log("hooson")
      if (bat) {
        let result = await axios({
          url: `/police/asap5411abhall`,
          method: "post",
          data: {
            username: "pu041092104",
          },
          // headers: {
          //   Authorization: `Bearer ${token}`, // Include the token in the Authorization header
          //   'X-Secret-Key': secretKey, // Include the secret key in a custom header
          // },
        });

        setListAll(result?.data?.data);
        setPages(result?.data?.pagination?.pagecount);
        
      } else {
        let result = await axios({
          url: `/police/asap5411abh`,
          method: "post",
          data: {
            username: "pu041092104",
          },
        });

        setList(result?.data?.data);
        setPages(result?.data?.pagination?.pagecount);
      }
    }
      setloading(false);
    } catch (err) {
      setloading(false);
      console.error("Error:", err.message);
    }
  };

  useEffect(() => {
    getData(); 
    //setChecked(bat); 
  }, [isChecked, itemsPerPage, bat,firstname]);


  useEffect(() => {
    const indexOfLastItem = currentPage * 10;
    const indexOfFirstItem = indexOfLastItem - 10;

    if (bat) {
      setLists(listAll.slice(indexOfFirstItem, indexOfLastItem));
    } else {
      setLists(list.slice(indexOfFirstItem, indexOfLastItem));
    }
  }, [isChecked, list, listAll, currentPage,bat,firstname]);
  console.log("bat.....",bat)
 
  console.log("firstname--",firstname)
  return (
<View style={styles.container}>
      <Header title={"Хайлт"} type={"menu"}/>
      <View style={styles.cardsContainer}>
                  <View style={styles.cardContainerGlue}>

                    <Ionicons
                      name="search"
                      size={35}
                      style={{ marginLeft: 1 }}
                      color="rgba(0,0,0,1)"
                    />
     <TouchableOpacity onPress={searchs}>
  <TextInput
    placeholder="Хайлт..."
    name="Register"                
    value={searchQuery}
    style={styles._textInputStyle}
    editable={false} // Set editable to false to make the input read-only
  />
</TouchableOpacity>
                      </View>
                      </View>
                      <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={hideModal}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
              <View style={styles.section}>
        </View>
        <View style={styles.cardContainer}>
        <View style={styles.cardTextContainer}>
                      <Text style={styles._textStyle}>Овог</Text>
                      <TextInput
                        placeholder="Эцэг/эхийн нэр"
                        keyboardType="default"
                        name="privacyNo"
                        maxLength={32}
                        returnKeyType={Platform.OS === "ios" ? "done" : "next"}
                        value={lastname}
                        onChange={(text) => {
                          setLastname(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                    </View>
                    <View style={styles.cardContainer}>
        <View
                      style={(styles.cardTextContainer, { marginLeft: 10 })}
                    >
                      <Text style={styles._textStyle}>Нэр</Text>
                      <TextInput
                        placeholder="Нэр"
                        maxLength={32}
                        keyboardType="default"
                        value={firstname}
                        returnKeyType={Platform.OS === "ios" ? "done" : "next"}
                        onChange={(text) => {
                          setFirstname(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                    </View>
                {/* <Text style={styles.modalText}>Бүх мэдээллийг харуулах</Text> */}
                <View style={styles.section}>
                <Pressable style={styles.buttonClose} onPress={handleUpdateData}>
                  <Text style={styles.textStyle}>Хайлт хийх</Text>
                </Pressable>
                <Pressable style={styles.buttonClose} onPress={hideModal}>
                  <Text style={styles.textStyle}>Хаах</Text>
                </Pressable>
                </View>
              </View>
            </View>
          </Modal>
      {loading ? (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          alignSelf: "center",
        }}
      >
        <ActivityIndicator size="large" color="#1c3587" />
      </View>
    ) : (
        <ScrollView>
        <View style={{ padding: 12, flexDirection: "column", flex: 1 }}>
          {item?.IMAGE != "" && imageLoader ? (
            <ActivityIndicator
              color="#1c3587"
              size="large"
              style={{ marginTop: 30 }}
            />
          ) : (
            image != false && (
              <TouchableOpacity
                onPress={() => {
                  setshowImage(true);
                }}
              >
                <Image
                  style={styles.logo}
                  resizeMode="contain"
                  source={{
                    uri: image,
                  }}
                />
              </TouchableOpacity>
            )
          )}
          <View style={styles.row}>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {item?.MTYPE + " "}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>ЭС зарлалтын №: {item?.M5400BNO}</Text>
            <Text style={styles.title}>Маягтын №: {item?.MBNO}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Хариуцсан алба хаагч</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.GAZARNAME + ", " + item?.WORKER + " " }
            </Text>
          </View>
          <View style={styles.row}>
            <Text style={{ fontSize: 15, }}>
              {"Шивсэн огноо: " + formattedDates + " "}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Алга болсон огноо</Text>
            <Text style={{ textAlign: "justify" }}>
              {/* {formattedDate1 + " - " + formattedDate2} */}
              {item?.MDATE}
            </Text>
            <Text style={styles.title}>Хүний мэдээлэл</Text>
            <Text style={{ textAlign: "justify" , color: 'blue', fontWeight:'600', fontSize:15}}>
              {/* { item?.LASTNAME?.toUpperCase() + " -н " + item?.FIRSTNAME?.toUpperCase() + " - "+ item?.SEX + ", " + item?.AGE + " настай"}
               */}   
               {item?.MINFO}
            </Text>
            </View>
          
          

          <View style={styles.minicon}>
            <Text style={styles.title}>Утга</Text>
            <Text style={{ textAlign: "justify" }}>{item?.M5400INFO}</Text>
          </View>
          {/* <View style={styles.minicon}>
            <Text style={styles.title}>Зэрэглэл</Text>
            {item?.ZEREGLEL_NAME == "Онц хүнд" ? (
              <Text style={{ color: "#f20039", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME + " "}
              </Text>
            ) : item?.ZEREGLEL_NAME === "Хүнд" ? (
              <Text style={{ color: "#ff543d", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME + " "}
              </Text>
            ) : item?.ZEREGLEL_NAME === "Хүндэвтэр" ? (
              <Text style={{ color: "#ff853d", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME + " "}
              </Text>
            ) : item?.ZEREGLEL_NAME === "Энгийн" ? (
              <Text style={{ color: "#C3941A", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME + " "}
              </Text>
            ) : (
              <View />
            )}
          </View> */}
          {/* <View style={styles.minicon}>
            <Text style={styles.title}>Хаанаас</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.BOPADDRESS + " "}
            </Text>
          </View> */}
          <View style={styles.minicon}>
            <Text style={styles.title}>Анхаарах зүйлс</Text>
            <Text style={{ textAlign: "justify" }}>{item?.M5400ANHAARAH + " "}</Text>
          </View>

          
          {/* <View style={styles.minicon}>
            <Text style={styles.title}>Байршлын мэдээлэл</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.AIMAG_NAME +
                "," +
                item?.SUM_NAME +
                " " +
                item?.HOROO +
                " баг/хороо \nХаяг : " +
                item?.ADDRESS +
                " "}
            </Text>
          </View> */}
          {loading ? (
            <ActivityIndicator
              color="#1c3587"
              size="large"
              style={{ marginTop: 30 }}
            />
          ) : dat ? (
            console.log("dat",dat)
            
          ) : (<TouchableOpacity
            style={{
              backgroundColor: "#1c3587",
              padding: 15,
              marginTop: 20,
              borderRadius: 5,
            }}
            onPress={() => {
              saw();
            }}
          >
            <Text style={{ color: "#FFf", textAlign: "center" }}>
              Танилцсан
            </Text>
          </TouchableOpacity>)}
        </View>
      </ScrollView>
    )}
    </View>

  )
}

export default HumanSearchScreen
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
  paginationContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom:10,
  },
  searchBarContainer: {
    backgroundColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
    marginHorizontal: 6,
       
    
  },
  textStyle: {
    paddingTop: 17,
    fontSize: 17,
    fontWeight: "500",
  },
  searchBarInput: {
    backgroundColor: 'white',
  
  },
  rowSpace: {
        marginHorizontal: 25,
        marginVertical: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    // borderWidth:1,
    // borderRadius:16,
    // width:300,
    // marginBottom:10,
    // paddingBottom:20
    // width: ScreenWidth - 80,
  },
  section: {
    marginHorizontal: 10,
    marginVertical: 4,
    flexDirection: 'row',
    alignItems: 'center',
    padding:5,
  },
  paragraph: {
    fontSize: 20,
  },
  switcher: {
    marginTop: 20,
    marginRight: 10,
  },
  checkbox: {
    margin: 8,
    borderRadius:8,
    width: 30,  // Set your desired width
  height: 30,
    
  },
  _textInputStyle: {
    fontSize: 17,
    color: "#000",
    fontWeight: "900",
    width: "100%",
    minWidth: "70%",
  },
  cardContainerGlue: {
    // flex: 1,
    marginLeft: 24,
    marginRight: 24,
    alignItems: "center",
    flexDirection: "row",
  },
  cardTextContainer: {
    flex: 1,
    marginLeft: 12,
    flexDirection: "column",
    justifyContent: "center",
    marginTop: Platform === "ios" ? null : 10,
  },
  cardsContainer: {
    //margin: 8,
    height: 55,
    width: "100%",
    marginTop: 0,
    //borderRadius: 24,
    borderBottomEndRadius:24,
    borderBottomStartRadius:24,
    justifyContent: "center",
    backgroundColor: "white",
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Adjust the opacity as needed
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '70%',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  _InputStyle: {
    fontSize: 14,
    color: "#000",
    fontWeight: "800",
  },
  _textStyle: {
    fontSize: 16,
    fontWeight: "700",
    color: "#000",
    // backgroundColor:'yellow',
  },
  buttonClose: {
    backgroundColor: '#1c3587',
    borderRadius:16,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    padding:8,
    //margin:8,
    paddingRight:12,
    paddingLeft:12
  },
  cardContainer: {
    margin: 8,
    height: 55,
    width: "100%",
    marginTop: 0,
    borderRadius: 20,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.1)",
  },
});
