import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  Alert,
  ActivityIndicator,
  Modal,
} from "react-native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import Header from "../../component/header";
import { DataContext } from "../../store/DataContext";
import { useRoute } from "@react-navigation/native";
import { getDownloadURL, getStorage,ref } from "firebase/storage";
import deviceStorage from "../../store/deviceStorage";

import axios from "axios";
import { useNavigation } from "@react-navigation/native";
import ImageViewer from "react-native-image-zoom-viewer";
import { MaterialCommunityIcons } from "@expo/vector-icons";
var dateFormat = require("dateformat");

const AsapDetailScreen = (datas) => {
  const route = useRoute();
  // Access the data passed from ZarlanScreen
  const dat = route.params?.saw
  const dataContext = useContext(DataContext);
 
 
  console.log("dkdkdkdk",dat)
  
  const BaseUrl = "http://www.rule.police.gov.mn";
  const [data, setData, theme, setTheme] = dataContext;
  const [image, setImage] = useState(false);
  const { item } = datas.route.params;
  const [loading, setloading] = useState(false);
  const [imageLoader, setimageLoader] = useState(false);
  const navigations = useNavigation();
  const storage = getStorage();
  const [showImage, setshowImage] = useState(false);
  const [harsan, setharsan] = useState(false);
  useEffect(() => {
    //getImage();
   setImage(item?.PIC_URL)
  }, []);
  
  // const getImage = async () => {
  //   console.log("imageref", item.IMAGE)
  //   try {
  //     setimageLoader(true);
      
  //     //let imageRef = ref(storage,`/zarlanIMG/${item.IMAGE}`);
      
  //     // let imageRef = storage.ref(`/zarlanIMG/${item.IMAGE}`);
      
  //     getDownloadURL(ref(storage,`/zarlanIMG/${item.IMAGE}`))
  //       .then((url) => {
  //         // console.log("URL", url);
  //         setImage(url);
  //         setimageLoader(false);
  //       })
  //       .catch((e) => console.log("getting downloadURL of image error => ", e));
  //   } catch (err) {
  //     console.log("err image", err);
  //   }
  // };
  const saw = async () => {
    try {
      setloading(true);
      let result = await axios({
        url: `/police/submitConfirm`,
        method: "post",
        data: {
          workHistoryID: data.user.WORKHISTORY_ID,
          BNo_ID: item?.BNO_ID,
          DeptCode: data.user.DEPTCODE,        
        },
      });
      //setharsan(result.data.isResult)
      //console.log("harsan",harsan)
      const rese = result.data.isResult;
      //console.log(rese)
      setharsan(rese)
      //console.log("ress",harsan)
      if (result?.data == "Амжилттай хадгаллаа")
        Alert.alert(
          "Мэдэгдэл",
          "Зарлан мэдээлэлтэй танилцсан мэдээлэл бүртгэгдлээ.",
          [
            {
              text: "Хаах",
              onPress: () =>
                navigations.replace("AsapList", {
                  currentdate: item?.CREATEDDATE,
                }),
              style: "cancel",
            },
          ],
          {
            cancelable: true,
            onDismiss: () =>
              navigations.replace("AsapList", {
                currentdate: item?.CREATEDDATE,
              }),
          }
        )
        else{//console.log("ressss",result.data)
        setloading(false);
        Alert.alert(
          "Мэдэгдэл",
          result.data,)};
    } catch (err) {
      setloading(false);
      Alert.alert("Алдаа 2", err.toString());
            if (err.response && err.response.status === 402) {
        deviceStorage.deleteStorage().then(() => {
      navigations.replace("Splash");
    });
      }
    }
  
  };
  return (
    <View style={{ flex: 1 }}>
      <Header title={"Зарлан дэлгэрэнгүй"} type={"back"} />
      <Modal visible={showImage} style={{ width: "100%", height: "100%" }}>
        <ImageViewer imageUrls={[{ url: image }]} />

        <MaterialCommunityIcons
          color={"#fff"}
          size={30}
          name="close"
          style={{
            position: "absolute",
            right: 20,
            top: 50,

            width: 30,
            height: 30,
          }}
          onPress={() => {
            setshowImage(false);
          }}
        />
      </Modal>
      <ScrollView>
        <View style={{ padding: 12, flexDirection: "column", flex: 1 }}>
          {item?.IMAGE != "" && imageLoader ? (
            <ActivityIndicator
              color="#1c3587"
              size="large"
              style={{ marginTop: 30 }}
            />
          ) : (
            image != false && (
              <TouchableOpacity
                onPress={() => {
                  setshowImage(true);
                }}
              >
                <Image
                  style={styles.logo}
                  resizeMode="contain"
                  source={{
                    uri: image,
                  }}
                />
              </TouchableOpacity>
            )
          )}
          <View style={styles.row}>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {item?.TYPE + " "}
            </Text>
          </View>
          <View style={styles.row}>
            <Text style={{ fontSize: 15, fontWeight: "700" }}>
              {item?.CREATEDDATE + " "}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Зарласан</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.DEPARTMENT + " "}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Утга</Text>
            <Text style={{ textAlign: "justify" }}>{item?.CONTENT + " "}</Text>
          </View>
          {/* <View style={styles.minicon}>
            <Text style={styles.title}>Зэрэглэл</Text>
            {item?.ZEREGLEL_NAME == "Онц хүнд" ? (
              <Text style={{ color: "#f20039", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME + " "}
              </Text>
            ) : item?.ZEREGLEL_NAME === "Хүнд" ? (
              <Text style={{ color: "#ff543d", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME + " "}
              </Text>
            ) : item?.ZEREGLEL_NAME === "Хүндэвтэр" ? (
              <Text style={{ color: "#ff853d", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME + " "}
              </Text>
            ) : item?.ZEREGLEL_NAME === "Энгийн" ? (
              <Text style={{ color: "#C3941A", fontSize: 14 }}>
                {item?.ZEREGLEL_NAME + " "}
              </Text>
            ) : (
              <View />
            )}
          </View> */}
          <View style={styles.minicon}>
            <Text style={styles.title}>Хаанаас</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.BOPADDRESS + " "}
            </Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Анхаарах зүйлс</Text>
            <Text style={{ textAlign: "justify" }}>{item?.ANHAARAHDESC + " "}</Text>
          </View>
          <View style={styles.minicon}>
            <Text style={styles.title}>Алба хаагч</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.WORKER + " "}
            </Text>
          </View>
          
          {/* <View style={styles.minicon}>
            <Text style={styles.title}>Байршлын мэдээлэл</Text>
            <Text style={{ textAlign: "justify" }}>
              {item?.AIMAG_NAME +
                "," +
                item?.SUM_NAME +
                " " +
                item?.HOROO +
                " баг/хороо \nХаяг : " +
                item?.ADDRESS +
                " "}
            </Text>
          </View> */}
          {loading ? (
            <ActivityIndicator
              color="#1c3587"
              size="large"
              style={{ marginTop: 30 }}
            />
          ) : dat ? (
            console.log("dat",dat)
            
          ) : (<TouchableOpacity
            style={{
              backgroundColor: "#1c3587",
              padding: 15,
              marginTop: 20,
              borderRadius: 5,
            }}
            onPress={() => {
              saw();
            }}
          >
            <Text style={{ color: "#FFf", textAlign: "center" }}>
              Танилцсан
            </Text>
          </TouchableOpacity>)}
        </View>
      </ScrollView>
    </View>
  );
};


export default AsapDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#Fff",
  },
  item: {
    flex: 1,
    textAlign: "left",
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    alignItems: "flex-start",
  },
  colorContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#788eec",
    height: 50,
    maxHeight: 50,
    minHeight: 50,
    // marginTop: isNotch ? 32 : 0
  },
  icon: {
    margin: 10,
  },
  title: {
    fontSize: 16,
    color: "black",
    fontWeight: "700",
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
  row: {
    paddingTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  minicon: {
    flexDirection: "column",
    paddingTop: 3,
  },
  logo: {
    width: "100%",

    height: 300,
  },
});

