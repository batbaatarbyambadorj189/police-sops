
import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
  Platform,
  Button,TouchableOpacity,TextInput,Switch
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsapList from "../../component/asap/asapList";
import Header from "../../component/header";
import Pagination from "../../component/Pagination";
import { DataContext } from "../../store/DataContext";
import axios from "axios";
import config from "../../config";
import Checkbox from 'expo-checkbox';
import deviceStorage from "../../store/deviceStorage";
const Asap5411HailtScreen = () => {
  const BaseUrl ="http://www.rule.police.gov.mn"
  const dataContext = useContext(DataContext);
  const [data, setData,theme, setTheme] = dataContext;
  const [loading, setloading] = useState(true);
  const [list, setList] = useState([]);
  const [listAll, setListAll] = useState([]);
  const navigations = useNavigation();
  const [currentPage, setCurrentPage] = useState(1);
  const [damjpage, setdamjPage] = useState(1);
  const [page, setPages] =useState(0);
  const [isChecked, setChecked] = useState(false);
  const itemsPerPage = 10
  const [lists, setLists] = useState([]);
  const [isEnabled, setIsEnabled] = useState(false);
  const [secretKey, setSecretKey] = useState(null);
  const toggleSwitch = () => setChecked(previousState => !previousState);
  const [bat, setBat] = useState(false); // State for the bat data

  // Function to update bat data
  const updateBat = (previousState) => {
    // Update the bat state with new data
    setBat(previousState);
  };
  const getData = async () => {
    
    try {
      setloading(true);
     
      
      if (bat) {
        let result = await axios({
          url: `/police/asap5411abhall`,
          method: "post",
          data: {
            username: "pu041092104",
          },
          // headers: {
          //   Authorization: `Bearer ${token}`, // Include the token in the Authorization header
          //   'X-Secret-Key': secretKey, // Include the secret key in a custom header
          // },
        });

        setListAll(result?.data?.data);
        setPages(result?.data?.pagination?.pagecount);
        
      } else {
        let result = await axios({
          url: `/police/asap5411abh`,
          method: "post",
          data: {
            username: data.user.USERNAMELOG,
          },
        });

        setList(result?.data?.data);
        setPages(result?.data?.pagination?.pagecount);
      }

      setloading(false);
    } catch (err) {
      setloading(false);
            if (err.response && err.response.status === 402) {
        deviceStorage.deleteStorage().then(() => {
      navigations.replace("Splash");
    });
      }
    }
  };

  useEffect(() => {
    getData(); 
    //setChecked(bat); 
  }, [isChecked, itemsPerPage, bat]);


  useEffect(() => {
    const indexOfLastItem = currentPage * 10;
    const indexOfFirstItem = indexOfLastItem - 10;

    if (bat) {
      setLists(listAll.slice(indexOfFirstItem, indexOfLastItem));
    } else {
      setLists(list.slice(indexOfFirstItem, indexOfLastItem));
    }
  }, [isChecked, list, listAll, currentPage,bat]);
  //console.log("bat.....",bat)
  return (
<View style={styles.container}>
      <Header title={"5411 Хайлт"} type={"menu"} category={"asap"} bat={bat} updateBat={updateBat}/>

      {loading ? (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          alignSelf: "center",
        }}
      >
        <ActivityIndicator size="large" color="#1c3587" />
      </View>
    ) : (
      <View style={{ flex: 1 }}>
              {/* <View style={styles.rowSpace}>
                  <Text
                    style={[
                      styles.textStyle,
                      theme === "white" ? { color: "#000" } : { color: "#FFF" },
                    ]}
                  >
                    Бүх мэдээлллийг харуулах{" "}
                  </Text>
                  <Switch
        
        thumbColor={isChecked ? '#1c3587' : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isChecked}
        style={styles.switcher}
        trackColor={{ false: "#000", true: "#fff" }}
        
      />
                </View> */}
                
      <FlatList
            data={lists}
            renderItem={({ item, index }) => (
              <AsapList item={item} />
            )}
            keyExtractor={(item, index) => item?.RN?.toString()}
          />

<View style={styles.paginationContainer}>
{page >0 &&<Pagination setCurrentPage={setCurrentPage} setPages={setPages} page={page} currentPage={currentPage} setdamjPage={setdamjPage}/> }

</View>
</View>
    )}
    </View>

  )
}

export default Asap5411HailtScreen
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
  paginationContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom:10,
  },
  searchBarContainer: {
    backgroundColor: 'transparent',
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
    marginHorizontal: 6,
       
    
  },
  textStyle: {
    paddingTop: 17,
    fontSize: 17,
    fontWeight: "500",
  },
  searchBarInput: {
    backgroundColor: 'white',
  
  },
  rowSpace: {
        marginHorizontal: 25,
        marginVertical: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    // borderWidth:1,
    // borderRadius:16,
    // width:300,
    // marginBottom:10,
    // paddingBottom:20
    // width: ScreenWidth - 80,
  },
  section: {
    marginHorizontal: 10,
    marginVertical: 4,
    flexDirection: 'row',
    alignItems: 'center',
    padding:5,
  },
  paragraph: {
    fontSize: 20,
  },
  switcher: {
    marginTop: 20,
    marginRight: 10,
  },
  checkbox: {
    margin: 8,
    borderRadius:8,
    width: 30,  // Set your desired width
  height: 30,
    
  },
});
