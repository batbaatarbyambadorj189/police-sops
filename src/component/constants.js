export const constant = {
    SPACING: 16,
    borderRadius: 10,
    titleFontSize: 24,
    textFontSize: 14,
    subTextFontSize: 14,
  };
  