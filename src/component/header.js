import React, { useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Platform, Modal, Pressable, Switch,TextInput
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { DataContext } from "../store/DataContext";
import { useNavigation } from "@react-navigation/native";
import Checkbox from 'expo-checkbox';
const header = (datas ) => {
  const dataContext = useContext(DataContext);
  const [modalVisible, setModalVisible] = useState(false);
  const [isChecked, setChecked] = useState(false);
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [data, setData, theme, setTheme] = dataContext;
  const navigation = useNavigation();
  const newBatData = 'new bat data'
  //const bat = 'bat';
  // const handleUpdateBat = () => {
  //  ; // Example: You can replace this with actual data
  //   datas?.updateBat(isChecked); 
  //   setModalVisible(false);// Update bat data in the parent screen
  // };
  // const handleUpdateFirstname = () =>{
  //   datas?.updateFirstname(firstname);
  //   setModalVisible(false);
  // }

  const handleUpdateData = () => {

    if (datas?.updateBat) {
      datas.updateBat(isChecked); // Update bat data if updateBat function exists
    }
    if (datas?.updateFirstname) {
      datas.updateFirstname(firstname); // Update firstname if updateFirstname function exists
    }
  
    setModalVisible(false); // Close the modal after updating data
  };
  
  const showModal = () => {
    setModalVisible(true);
    //console.log("bdata bat",datas.bat)
    if(datas?.bat == false){
      setChecked(false)
    }
  };

  const hideModal = () => {
    setModalVisible(false);
  };
  const toggleSwitch = () => setChecked(previousState => !previousState);
  return (
    <>
    
      <View
        style={[
          styles.notch,
          theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.8)" },
        ]}
      ></View>
      <View
        style={[
          styles.container,
          theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.8)" },
          //modalVisible ? styles.containerModalOpen : {},
        ]}
      >
        
        <View style={{ flexDirection: "row" }}>
          {datas?.type == "menu" ? (
            <TouchableHighlight
              onPress={() => {
                navigation.openDrawer();
              }}
            >
              <MaterialCommunityIcons
                size={30}
                color="white"
                name="menu"
                style={styles.icon}
              />
            </TouchableHighlight>
          ) : (
            <TouchableHighlight
              onPress={() => {
                navigation.goBack();
              }}
            >
              <MaterialCommunityIcons
                size={30}
                color="white"
                name="arrow-left"
                style={styles.icon}
              />
            </TouchableHighlight>
          )}
          <TouchableHighlight
            onPress={() => {
              datas?.type == "menu"
                ? navigation.openDrawer()
                : navigation.goBack();
            }}
            style={{flexWrap:"wrap"}}
          >
            <Text style={styles.title}>{datas?.title}</Text>
          </TouchableHighlight>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-end", flex: 1 }}>
          {datas?.category == "asap" && (
            <TouchableHighlight
              onPress={() => {
                showModal();
              }}
            >
              <MaterialCommunityIcons
                size={30}
                color="white"
                name="filter-menu-outline"
                style={styles.icon}
              />
            </TouchableHighlight>
          ) }
           <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={hideModal}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
              <View style={styles.section}>
              <Switch
        
        thumbColor={isChecked ? '#1c3587' : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isChecked}
        style={styles.switcher}
        trackColor={{ false: "#000", true: "#fff" }}
        
      />
        <Text style={styles.paragraph}>Бүх мэдээллийг харуулах</Text>
        
        </View>
        <View style={styles.cardContainer}>
        <View style={styles.cardTextContainer}>
                      <Text style={styles._textStyle}>Овог</Text>
                      <TextInput
                        placeholder="Эцэг/эхийн нэр"
                        keyboardType="default"
                        name="privacyNo"
                        maxLength={32}
                        returnKeyType={Platform.OS === "ios" ? "done" : "next"}
                        value={lastname}
                        onChange={(text) => {
                          setLastname(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                    </View>
                    <View style={styles.cardContainer}>
        <View
                      style={(styles.cardTextContainer, { marginLeft: 10 })}
                    >
                      <Text style={styles._textStyle}>Нэр</Text>
                      <TextInput
                        placeholder="Нэр"
                        maxLength={32}
                        keyboardType="default"
                        value={firstname}
                        returnKeyType={Platform.OS === "ios" ? "done" : "next"}
                        onChange={(text) => {
                          setFirstname(text.nativeEvent.text);
                        }}
                        style={styles._textInputStyle}
                      />
                    </View>
                    </View>
                {/* <Text style={styles.modalText}>Бүх мэдээллийг харуулах</Text> */}
                <View style={styles.section}>
                <Pressable style={styles.buttonClose} onPress={handleUpdateData}>
                  <Text style={styles.textStyle}>Хайлт хийх</Text>
                </Pressable>
                <Pressable style={styles.buttonClose} onPress={hideModal}>
                  <Text style={styles.textStyle}>Хаах</Text>
                </Pressable>
                </View>
              </View>
            </View>
          </Modal>
        </View>
        {datas?.isSave ? (
          <TouchableHighlight
            onPress={() => {
              datas?.functions();
            }}
          >
            <MaterialCommunityIcons
              size={30}
              color="white"
              name="content-save-outline"
              style={styles.icon}
            />
          </TouchableHighlight>
        ) : datas?.isDelete ? (
          <TouchableHighlight
            onPress={() => {
              datas?.functions();
            }}
          >
            <MaterialCommunityIcons
              size={30}
              color="white"
              name="delete"
              style={styles.icon}
            />
          </TouchableHighlight>
        ) : (
          <View />
        )} 
      </View>
    </>
  );
};

export default header;

const styles = StyleSheet.create({
  notch: {
    backgroundColor: "#1c3587",
    height: Platform === "ios" ? 40 : 28,
  },
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#1c3587",
    height: 60,
    paddingTop: 10,
    maxHeight: 60,
    minHeight: 60,
    // marginTop: isNotch ? 32 : 0
  },
  icon: {
    margin: 10,
  },
  title: {
 
    fontSize: 20,
    paddingTop: 13,
    color: "white",
    fontWeight: "500",

  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#1c3587',
    borderRadius:16,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    padding:8,
    //margin:8,
    paddingRight:12,
    paddingLeft:12
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Adjust the opacity as needed
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '70%',
  },
  cardTextContainer: {
    flex: 1,
    marginLeft: 12,
    flexDirection: "column",
    justifyContent: "center",
    marginTop: Platform === "ios" ? null : 10,
  },
  _textInputStyle: {
    fontSize: 16,
    color: "#000",
    fontWeight: "900",
    width: "100%",
    minWidth: "70%",
    paddingBottom:6
  },
  _InputStyle: {
    fontSize: 14,
    color: "#000",
    fontWeight: "800",
  },
  _textStyle: {
    fontSize: 16,
    fontWeight: "700",
    color: "#000",
    // backgroundColor:'yellow',
  },
  cardContainer: {
    margin: 8,
    height: 55,
    width: "100%",
    marginTop: 0,
    borderRadius: 20,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.1)",
  },
});
