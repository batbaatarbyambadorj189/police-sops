import { View, Text,StyleSheet } from 'react-native';
import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';

const Pagination = ({ setCurrentPage,setPages,page,currentPage,setdamjPage }) => {
 //console.log("page",page)
//console.log("currentPage",currentPage)
  //Set number of pages
  const numberOfPages = []
  for (let i = 1; i <= page; i++) {
    numberOfPages.push(i)
  }
 //console.log("ssssss",numberOfPages)
  // Current active button number
  const [currentButton, setCurrentButton] = useState(currentPage)
  // Array of buttons what we see on the page
  const [arrOfCurrButtons, setArrOfCurrButtons] = useState([1, 2, 3, 4, '...', numberOfPages.length])
 //console.log("ehnii",arrOfCurrButtons)
  useEffect(() => {
    let tempNumberOfPages = [...arrOfCurrButtons]

    let dotsInitial = '...'
    let dotsLeft = '... '
    let dotsRight = ' ...'

    if (numberOfPages.length < 6) {
      tempNumberOfPages = numberOfPages
    }

    else if (currentButton >= 1 && currentButton <= 3) {
      tempNumberOfPages = [1, 2, 3, 4, dotsInitial, numberOfPages.length]
    }

    else if (currentButton === 4) {
      const sliced = numberOfPages.slice(0, 5)
      tempNumberOfPages = [...sliced, dotsInitial, numberOfPages.length]
    }

    else if (currentButton > 4 && currentButton < numberOfPages.length - 2) {               // from 5 to 8 -> (10 - 2)
      const sliced1 = numberOfPages.slice(currentButton - 2, currentButton)                 // sliced1 (5-2, 5) -> [4,5] 
      const sliced2 = numberOfPages.slice(currentButton, currentButton + 1)                 // sliced1 (5, 5+1) -> [6]
      tempNumberOfPages = ([1, dotsLeft, ...sliced1, ...sliced2, dotsRight, numberOfPages.length]) // [1, '...', 4, 5, 6, '...', 10]
    }
    
    else if (currentButton > numberOfPages.length - 3) {                 // > 7
      const sliced = numberOfPages.slice(numberOfPages.length - 4)       // slice(10-4) 
      tempNumberOfPages = ([1, dotsLeft, ...sliced])                        
    }
    
    else if (currentButton === dotsInitial) {
      // [1, 2, 3, 4, "...", 10].length = 6 - 3  = 3 
      // arrOfCurrButtons[3] = 4 + 1 = 5
      // or 
      // [1, 2, 3, 4, 5, "...", 10].length = 7 - 3 = 4
      // [1, 2, 3, 4, 5, "...", 10][4] = 5 + 1 = 6
      setCurrentButton(arrOfCurrButtons[arrOfCurrButtons.length-3] + 1) 
    }
    else if (currentButton === dotsRight) {
      setCurrentButton(arrOfCurrButtons[3] + 2)
    }

    else if (currentButton === dotsLeft) {
      setCurrentButton(arrOfCurrButtons[3] - 2)
    }

    setArrOfCurrButtons(tempNumberOfPages)
    
    setCurrentPage(currentButton);
    //Pagination();
  }, [currentButton,numberOfPages.length])
//console.log("buttons", arrOfCurrButtons)

  return (
    <View style={styles.paginationContainer}>
    <TouchableOpacity
    disabled={currentButton === 1}
    onPress={() => setCurrentButton(prev => (prev <= 1 ? prev : prev - 1))}
  >
    <Text style={currentButton === 1 ? styles.paginationLinkDisabled : styles.paginationLink}>
      {"<"}
    </Text>
  </TouchableOpacity>

  {arrOfCurrButtons.map((item, index) => (
    <TouchableOpacity key={index} onPress={() => setCurrentButton(item)}>
      <Text style={currentButton === item ? styles.paginationLinkActive : styles.paginationLink}>
        {item}
      </Text>
    </TouchableOpacity>
  ))}

  <TouchableOpacity
    disabled={currentButton === numberOfPages.length}
    onPress={() =>
      setCurrentButton(prev => (prev >= numberOfPages.length ? prev : prev + 1))
    }
  >
    <Text
      style={
        currentButton === numberOfPages.length
          ? styles.disabledText
          : styles.paginationLink
      }
    >
      {">"}
    </Text>
  </TouchableOpacity>
      </View>
  );
}

export default Pagination
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
      borderBottomWidth: 1,
    },
    noInternet: {
      flex: 1,
      justifyContent: "center",
    },
    paginationContainers: {
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      marginTop: 10,
    },
    
    paginationContainer: {
      paddingBottom:10,
      flexDirection: "row",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        /* backgroundColor: 'rgba(216, 73, 73, 0.2)', */
        width: '100%',
        fontWeight: 500,
        fontSize: 15,
        borderRadius:8,
      },
      paginationLink: {
        justifyContent: 'center',
  alignItems: 'center',
  color: 'black',
  padding: 8, // Adjust padding as needed
  margin: 3, // Add margin between buttons
  height: 35,
  width: 35,
  textDecorationLine: 'none',
  
  borderWidth: 1,
  borderColor: 'blue',
  borderRadius: 8,
  
      },
      paginationLinkActive: {
        backgroundColor: '#007bff',
        
        //shadowRadius:8,
        textAlign:'center',
        padding: 4,
        justifyContent: 'center',
        alignItems: 'center',
        //color: 'blue',
        borderRadius: 8,
        margin: 4, 
        height: 30,
        overflow:'hidden',
  width: 30,
  borderWidth: 1,

      },
      paginationLinkDisabled: {
        opacity: 0.2,
      },
      paginationLinkHover: {
        backgroundColor: 'rgb(238, 238, 238)',
      },
    });
  
  