import React, { useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Platform,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { DataContext } from "../store/DataContext";
import { useNavigation } from "@react-navigation/native";
const header = (datas) => {
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const navigation = useNavigation();
  return (
    <>
      <View
        style={[
          styles.notch,
          theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.8)" },
        ]}
      ></View>
      <View
        style={[
          styles.container,
          theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.8)" },
        ]}
      >
        <View style={{ flexDirection: "row" }}>
          {datas?.type == "menu" ? (
            <TouchableHighlight
              onPress={() => {
                navigation.openDrawer();
              }}
            >
              <MaterialCommunityIcons
                size={30}
                color="white"
                name="menu"
                style={styles.icon}
              />
            </TouchableHighlight>
          ) : (
            <TouchableHighlight
              onPress={() => {
                navigation.goBack();
              }}
            >
              <MaterialCommunityIcons
                size={30}
                color="white"
                name="arrow-left"
                style={styles.icon}
              />
            </TouchableHighlight>
          )}
          <TouchableHighlight
            onPress={() => {
              datas?.type == "menu"
                ? navigation.openDrawer()
                : navigation.goBack();
            }}
          >
            <Text style={styles.title}>{datas?.title}</Text>
          </TouchableHighlight>
        </View>
        {datas?.isSave ? (
          <TouchableHighlight
            onPress={() => {
              datas?.functions();
            }}
          >
            <MaterialCommunityIcons
              size={30}
              color="white"
              name="content-save-outline"
              style={styles.icon}
            />
          </TouchableHighlight>
        ) : datas?.isDelete ? (
          <TouchableHighlight
            onPress={() => {
              datas?.functions();
            }}
          >
            <MaterialCommunityIcons
              size={30}
              color="white"
              name="delete"
              style={styles.icon}
            />
          </TouchableHighlight>
        ) : (
          <View />
        )}
      </View>
    </>
  );
};

export default header;

const styles = StyleSheet.create({
  notch: {
    backgroundColor: "#1c3587",
    height: Platform === "ios" ? 40 : 28,
  },
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#1c3587",
    height: 60,
    paddingTop: 10,
    maxHeight: 60,
    minHeight: 60,
    // marginTop: isNotch ? 32 : 0
  },
  icon: {
    margin: 10,
  },
  title: {
    fontSize: 20,
    paddingTop: 13,
    color: "white",
    fontWeight: "500",
  },
});
