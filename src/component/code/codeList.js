import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  Animated,
  StyleSheet,
  TouchableHighlight,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { DataContext } from "../../store/DataContext";
const codeList = (datas) => {
  const dataContext = useContext(DataContext);
  const navigation = useNavigation();
  const [data, setData, theme, setTheme] = dataContext;
  return (
    <TouchableHighlight
      activeOpacity={0.9}
      underlayColor={theme === "white" ? "#1c3587" : "rgba(0,0,0,0.5)"}
      style={[
        {
          marginVertical: 4,
          borderRadius: 10,
          backgroundColor: "#fff",
          borderWidth: 0.3,
          width: "94%",
          marginHorizontal: "3%",
          borderWidth: 0.3,
          borderColor: "#ddd",
          borderBottomWidth: 1,
          shadowColor: "#000",
          shadowOffset: { width: 1, height: 1 },
          shadowOpacity: 0.3,
          shadowRadius: 1,
          elevation: 1,
          height: 70,
        },
        theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.7)" },
      ]}
      onPress={() => {
        navigation.navigate("CodeList", {
          id: datas?.item?.RULETYPE_ID,
          title: datas?.item?.RULETYPENAME,
        });
      }}
    >
      <View style={styles.row}>
        <Text
          key={datas.item.index}
          style={[
            {
              flex: 1,
              margin: 5,
              // fontSize: 0.042 * ScreenWidth,
              marginLeft: 25,
              textAlign: "left",
              justifyContent: "center",
              alignItems: "center",
              alignContent: "center",
              alignSelf: "center",
            },
            theme === "white" ? {} : { color: "#FFF" },
          ]}
        >
          {datas?.type == "group"
            ? datas?.item?.RULETYPENAME
            : datas?.item?.RULETITLE?.charAt(0).toUpperCase() +
              datas?.item?.RULETITLE?.slice(1)}
        </Text>
      </View>
    </TouchableHighlight>
  );
};

export default codeList;
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginVertical: 4,
    borderRadius: 8,
    padding: 8,
    borderColor: "rgba(0,0,0,5)",
    borderWidth: 0.2,
  },
  row: {
    flex: 1,
    flexDirection: "row",
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    color: "black",
  },
  paragraph: {
    flex: 1,
    margin: 5,
    fontSize: 12,
    fontWeight: "bold",
    textAlign: "left",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    alignSelf: "center",
  },
  icon: {
    alignItems: "center",
    alignContent: "center",
    alignSelf: "center",
    marginHorizontal: 10,
  },
});
