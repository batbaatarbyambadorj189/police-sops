import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  Animated,
  StyleSheet,
  TouchableHighlight,
  useWindowDimensions,
} from "react-native";
import { DataContext } from "../../store/DataContext";
// import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
const codeItem = (datas) => {
  const dimensions = useWindowDimensions();
  const dataContext = useContext(DataContext);
  const navigation = useNavigation();
  const [data, setData, theme, setTheme] = dataContext;
  return (
    <TouchableHighlight
      activeOpacity={0.9}
      underlayColor={theme === "white" ? "#1c3587" : "rgba(0,0,0,0.5)"}
      style={[
        {
          marginVertical: 4,
          borderTopLeftRadius: 30,
          borderBottomRightRadius: 30,
          backgroundColor: "#fff",
          borderWidth: 0.3,
          width: "94%",
          marginHorizontal: "3%",
          borderWidth: 0.3,
          borderColor: "#ddd",
          borderBottomWidth: 1,
          shadowColor: "#000",
          shadowOffset: { width: 1, height: 1 },
          shadowOpacity: 0.3,
          shadowRadius: 1,
          elevation: 1,
          height: 75,
        },
        theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.7)" },
      ]}
      onPress={() => {
        navigation.push("CodeDetail", { item: datas?.item });
      }}
    >
      <View style={styles.row}>
        <View
          style={{
            width: 70,
            maxHeight: 70,
            height: 70,
            borderRadius: 8,
            borderColor: "#ddd",
          }}
        >
          <Text
            style={[
              {
                flex: 1,
                justifyContent: "center",
                alignContent: "center",
                alignSelf: "center",
                alignItems: "center",
                paddingTop: 25,
                paddingLeft: 5,
                fontSize: 0.04 * dimensions.width,
                fontWeight: "400",
              },
              theme === "white" ? {} : { color: "#FFF" },
              // datas?.item?.RULENUMBER.length < 6
              // ? { paddingTop: 25, paddingLeft: 5 }
              // : { paddingTop: 12, paddingLeft: 15 },
              // ? { fontSize: 0.04 * dimensions.width }
              // : { fontSize: 0.02 * dimensions.width },
            ]}
          >
            {datas?.item?.RULENUMBER.substr(0, 5).replace(",", "")}
          </Text>
        </View>
        <View
          style={[
            {
              backgroundColor: "#1c3587",
              width: 1,
              height: 50,
              marginVertical: 10,
            },
            theme === "white"
              ? {}
              : { backgroundColor: "rgba(255,255,255,0.5)" },
          ]}
        />
        <Text
          key={datas.item.index}
          numberOfLines={4}
          style={[
            {
              flex: 1,
              margin: 5,
              fontSize: 12,
              marginLeft: 15,
              textAlign: "left",
              justifyContent: "center",
              alignItems: "center",
              alignContent: "center",
              alignSelf: "center",
            },
            theme === "white" ? {} : { color: "#FFF" },
          ]}
        >
          {datas?.type == "group"
            ? datas?.item?.RULETYPENAME
            : datas?.item?.RULETITLE?.charAt(0).toUpperCase() +
              datas?.item?.RULETITLE?.slice(1)}
        </Text>
      </View>
    </TouchableHighlight>
  );
};

export default codeItem;
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginVertical: 4,
    borderRadius: 8,
    padding: 8,
    borderColor: "rgba(0,0,0,5)",
    borderWidth: 0.2,
  },
  row: {
    flex: 1,
    flexDirection: "row",
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    color: "black",
  },
  paragraph: {
    flex: 1,
    margin: 5,
    fontSize: 12,
    fontWeight: "bold",
    textAlign: "left",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    alignSelf: "center",
  },
});
