
import React, {useContext} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import PropTypes from 'prop-types'

const InfoText = ({ text }) => (
  
  <View style={styles.container}>
    <Text style={styles.infoText}>{text}</Text>
  </View>
)
const styles = StyleSheet.create({
    container: {
      paddingTop: 20,
      paddingBottom: 12,
    },
    infoText: {
      fontSize: 16,
      marginLeft: 6,
      color: 'gray',
      fontWeight: '500',
    },
  })
export default InfoText