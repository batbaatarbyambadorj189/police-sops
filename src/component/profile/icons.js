import React, { useContext } from 'react'
import { StyleSheet, View, Text, TouchableHighlight, AsyncStorage ,useWindowDimensions} from 'react-native'
import { FontAwesome } from '@expo/vector-icons';
import { DataContext } from '../../store/DataContext';
const BaseIcon = ({ containerStyle, icon, text, isDivide, disable, did }) => {
  const dataContext = useContext(DataContext);
  const [data,setData,theme, setTheme] = dataContext;
  const dimensions = useWindowDimensions();
  return (<TouchableHighlight activeOpacity={0.9}
    underlayColor="#788eec" disabled={disable} onPress={
      did

    }>
    <View style={styles.big}>
      <View style={styles.rowStyle}>
        <View style={[styles.container, containerStyle]}>
          <FontAwesome
            size={20}
            color="white"
            // name='music'
            {...icon}
          />
        </View>
        <Text style={[styles.textStyle,{maxWidth:dimensions.width-55}, theme === "white" ? {color:'black'} : {color:'white'} ]}>{text}</Text>
      </View>
      {
        isDivide == true ?
        <View style={[styles.divider,theme === "white" ? {backgroundColor: "rgba(0,0,0,0.2)"} :  {backgroundColor: "rgba(255,255,255,0.5)"}]}></View>
          :
          <View></View>
      }
    </View>
  </TouchableHighlight>)
}
export default BaseIcon
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'black',
    borderColor: 'transparent',
    borderRadius: 10,
    borderWidth: 1,
    height: 34,
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 18,
    width: 34,
    marginTop: 10,
    marginBottom: 10
  },
  big: {
    flexDirection: 'column'
  },
  rowStyle: {
    flexDirection: 'row',
  },
  textStyle: {
    justifyContent:'center',
    alignContent:'center',
    alignSelf:'center',
    fontSize: 17,
    fontWeight: '500',
    flexWrap: 'wrap',
    alignItems: 'center',
    textAlign: 'left'
  },
  divider: {
    backgroundColor: "rgba(0,0,0,0.2)",
    height: 1,
    marginBottom: 5
  }
})