import { useNavigation } from "@react-navigation/core";
import React, { useContext, useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ActivityIndicator,
  Alert,
} from "react-native";
import { DataContext } from "../../store/DataContext";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import axios from "axios";
var dateFormat = require("dateformat");
const zarlan = (datas) => {
  const navigation = useNavigation();
  const BaseUrl = "http://www.rule.police.gov.mn";
  const dataContext = useContext(DataContext);
  const [data, setData, theme, setTheme] = dataContext;
  const [loading, setloading] = useState(false);
  const [saw, setsaw] = useState(false);
  let { item } = datas;
  // console.log('====================================');
  // console.log("item", item);
  // console.log('====================================');
  useEffect(() => {
    (async () => {
      try {
        setloading(true);
        let result = await axios({
          url: `/police/sawcheck`,
          method: "post",
          data: {
            id: item.ID,
            username: data.user.USERNAMELOG,
          },
        });
        //console.log("result", item);
        setsaw(result.data.saw);
        setloading(false);
      } catch (err) {
        setloading(false);
        Alert.alert("Алдаа --", err.toString());
      }
    })();
  }, []);
  return (
    <TouchableHighlight
      activeOpacity={0.9}
      underlayColor="#788eec"
      style={{
        marginVertical: 4,
        borderRadius: 15,
        backgroundColor: "#fff",
        borderWidth: 0.3,
        width: "94%",
        marginHorizontal: "3%",
        borderWidth: 0.3,
        borderColor: "#ddd",
        borderBottomWidth: 1,
        shadowColor: "#000",
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.3,
        shadowRadius: 1,
        elevation: 1,
      }}
      onPress={() => {
        navigation.navigate("ZarlanDetail", { item, setsaw, saw });
      }}
    >
      <View style={styles.container}>
        <Text style={styles.header}>{item?.ZARLAN_TYPE_NAME}</Text>
        <Text style={{ textAlign: "justify" }} numberOfLines={4}>
          {item?.CONTENT}
        </Text>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            height: 22,
            justifyContent: "space-between",
          }}
        >
          {loading ? (
            <ActivityIndicator size="small" color="blue" />
          ) : saw == true ? (
            <MaterialCommunityIcons name="check" size={24} color="green" />
          ) : (
            <View />
          )}
          <Text style={{ color: "blue", fontWeight: "700" }}>Дэлгэрэнгүй</Text>
        </View>
        <View style={styles.row}>
          {item?.ZEREGLEL_NAME === "Онц хүнд" ? (
            <Text style={{ color: "#f20039", fontSize: 14 }}>
              {item?.ZEREGLEL_NAME}
            </Text>
          ) : item?.ZEREGLEL_NAME === "Хүнд" ? (
            <Text style={{ color: "#cc4b00", fontSize: 14 }}>
              {item?.ZEREGLEL_NAME}
            </Text>
          ) : item?.ZEREGLEL_NAME === "Хүндэвтэр" ? (
            <Text style={{ color: "#ff853d", fontSize: 14 }}>
              {item?.ZEREGLEL_NAME}
            </Text>
          ) : item?.ZEREGLEL_NAME === "Энгийн" ? (
            <Text style={{ color: "#C3941A", fontSize: 14 }}>
              {item?.ZEREGLEL_NAME}
            </Text>
          ) : (
            <View />
          )}
          <Text>{item?.CREATEDDATE}</Text>
        </View>
      </View>
    </TouchableHighlight>
  );
};

export default zarlan;
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginVertical: 4,

    padding: 8,
  },
  row: {
    paddingTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  header: {
    fontWeight: "700",
    fontSize: 15,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
  },
  noInternet: {
    flex: 1,
    justifyContent: "center",
  },
});
