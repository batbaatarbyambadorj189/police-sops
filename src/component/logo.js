import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  useWindowDimensions,
} from "react-native";

const logo = () => {
  const dimestions= useWindowDimensions();
  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <View style={styles.iconStyle}>
          <Image
            style={styles.logoStyle}
            source={require("../../assets/logo.png")}
          />
        </View>
        <View style={styles.logoContainer}>
          <Text style={[styles.logoText, { fontSize: dimestions.width*0.025 }]}>
            МОНГОЛ УЛСЫН ЗАСГИЙН ГАЗРЫН ТОХИРУУЛАГЧ АГЕНТЛАГ
          </Text>
          <Text style={[styles.policeText,{fontSize:dimestions.width*0.05}]}>ЦАГДААГИЙН ЕРӨНХИЙ ГАЗАР</Text>
        </View>
      </View>
    </View>
  );
};

export default logo;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
  },
  textStyle: {
    fontSize: 20,
    color: "white",
  },
  row: {
    alignItems: "center",
    flexDirection: "column",
  },
  iconStyle: {},
  logoStyle: {
    width: 125,
    height: 125,
  },
  logoContainer: {
    alignItems: "center",
  },
  logo: {
    width: 100,
    height: 100,
  },
  logoText: {
    color: "white",
    fontWeight: "500",
    fontSize: 10,
    justifyContent: "center",
    marginTop: 10,
  },
  policeText: {
    color: "white",
    fontSize: 20,
    fontWeight: "500",
    justifyContent: "center",
    marginTop: -2,
  },
});
