// import React, { useState } from 'react';
// import { View, TextInput, FlatList, Text, Button } from 'react-native';
// import BottomSheet from 'reanimated-bottom-sheet';

// const SearchInBottomSheet = ({ data, onItemPress }) => {
//   const [query, setQuery] = useState('');
//   const [filteredData, setFilteredData] = useState(data);

//   const handleSearch = (text) => {
//     const filtered = data.filter((item) =>
//       item.toLowerCase().includes(text.toLowerCase())
//     );
//     setQuery(text);
//     setFilteredData(filtered);
//   };

//   const handlePress = (item) => {
//     onItemPress(item);
//     sheetRef.current.snapTo(2); // Close the bottom sheet after selection
//   };

//   const renderContent = () => (
//     <View style={{ backgroundColor: 'white', padding: 16, height: 450 }}>
//       <TextInput
//         style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10, paddingHorizontal: 10 }}
//         placeholder="Search..."
//         value={query}
//         onChangeText={handleSearch}
//       />
//       <FlatList
//         data={filteredData}
//         renderItem={({ item }) => (
//           <Text onPress={() => handlePress(item)}>{item}</Text>
//         )}
//         keyExtractor={(item, index) => index.toString()}
//       />
//     </View>
//   );

//   const sheetRef = React.useRef(null);

//   return (
//     <View style={{}}>
//       <Button
//         title="Open Bottom Sheet"
//         onPress={() => sheetRef.current.snapTo(1)}
//       />
//       <BottomSheet
//         ref={sheetRef}
//         snapPoints={[450, 300, 0]}
//         borderRadius={10}
//         renderContent={renderContent}
//         initialSnap={2}
//       />
//     </View>
//   );
// };

// export default SearchInBottomSheet;
