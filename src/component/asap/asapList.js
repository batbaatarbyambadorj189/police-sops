import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  Animated,
  StyleSheet,
  TouchableHighlight,
  Alert,
  ActivityIndicator,
  Image
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";
import { DataContext } from "../../store/DataContext";
import { useFocusEffect } from "@react-navigation/native";
import deviceStorage from "../../store/deviceStorage";
const codeList = (datas) => {
  const dataContext = useContext(DataContext);
  const navigation = useNavigation();
  const [asapsaw, setasapsaw] = useState(false);
  const [loading, setloading] = useState(false);
  const [data, setData, theme, setTheme] = dataContext;
  const BaseUrl = "http://www.rule.police.gov.mn";
  let { item } = datas;
  //console.log("hmm",datas);
  const date = new Date(datas?.item?.CREATEDDATE);

  useFocusEffect(
    React.useCallback(() => {
      const fetchData = async () => {
        try {
          setloading(true);
          let result = await axios({
            url: `/police/sawAsap`,
            method: "post",
            data: {
              workHistoryID: data.user.WORKHISTORY_ID,
              BNo_ID: item?.BNO_ID,
              username: data.user.USERNAMELOG,  
            },
          });
          //console.log("result", result.data.l);
          setasapsaw(result.data.l);
          setloading(false);
        } catch (err) {
          setloading(false);
          Alert.alert("Алдаа --", err.toString());
                if (err.response && err.response.status === 402) {
        deviceStorage.deleteStorage().then(() => {
      navigation.replace("Splash");
    });
      }
        }
      };
  
      fetchData(); // Call the fetchData function when the screen gains focus
  
      // Optional cleanup function if needed
      return () => {
        // Cleanup logic here
      };
    }, [data.user.WORKHISTORY_ID, item?.BNO_ID, data.user.USERNAMELOG]) // Include dependencies here
  );

  // useEffect(() => {
  //   (async () => {
  //     try {
  //       setloading(true);
  //       let result = await axios({
  //         url: `/police/sawAsap`,
  //         method: "post",
  //         data: {
  //           workHistoryID: data.user.WORKHISTORY_ID,
  //           BNo_ID: item?.BNO_ID,
  //           username: data.user.USERNAMELOG,  
  //         },
  //       });
  //       console.log("result", result.data.l);
  //       setasapsaw(result.data.l);
  //       setloading(false);
  //     } catch (err) {
  //       setloading(false);
  //       Alert.alert("Алдаа --", err.toString());
  //     }
  //   })();
  // }, []);
  
// Get the date portion (YYYY-MM-DD)
const formattedDate = date.toISOString().split('T')[0];
  return (
//     <TouchableHighlight
//     activeOpacity={0.9}
//     underlayColor="#788eec"
//     style={{
//       marginVertical: 4,
//       borderRadius: 15,
//       backgroundColor: "#fff",
//       borderWidth: 0.3,
//       width: "94%",
//       marginHorizontal: "3%",
//       borderWidth: 0.3,
//       borderColor: "#ddd",
//       borderBottomWidth: 1,
//       shadowColor: "#000",
//       shadowOffset: { width: 1, height: 1 },
//       shadowOpacity: 0.3,
//       shadowRadius: 1,
//       elevation: 1,

//     }}
//     onPress={() => {
//       navigation.navigate("AsapDetail", { item, setasapsaw,asapsaw});
//     }}
//   >
//     <View style={styles.container}>
      
//     <View 
//   style={{
//     flex: 1,
//     flexDirection: "row",
//     //justifyContent: 'flex-start',
//   }}
// >
//   <View 
//     style={{
//       flex: 1,
//       flexDirection: "column",
//        //justifyContent: 'flex-end',
//     }}
//   >
//   <Image
//     style={styles.logo}
//     resizeMode="contain"
//     source={{
//       uri: item?.MPICURL,
//     }}
//   />

// <Text style={{color:'blue'}}>{ item?.REGISTER?.toUpperCase()}</Text>
// </View>
//   <View 
//     style={{
//       flex: 1,
//       flexDirection: "column",
//       //justifyContent: 'flex-start',
//     }}
//   >
//     <Text style={styles.header}>{datas?.item?.MTYPE}</Text>
//     <Text style={{color:'blue'}}>{ item?.MINFO}</Text>
//     <Text style={{ textAlign: "justify" }} numberOfLines={4}>
//       {datas?.item?.M5400INFO}
//     </Text>
//   </View>
// </View>
// {/* <View
//           style={{
//             flex: 1,
//             flexDirection: "row",
//             height: 22,
//             justifyContent: "space-between",
//           }}
//         >
// <Text style={{color:'blue'}}>{ item?.REGISTER?.toUpperCase()}</Text>

// </View> */}
//         <View
//           style={{
//             flex: 1,
//             flexDirection: "row",
//             height: 22,
//             justifyContent: "space-between",
//           }}
//         >
//           {loading ? (
//             <ActivityIndicator size="small" color="blue" />
//           ) : asapsaw == true ? (
//             <MaterialCommunityIcons name="check" size={24} color="green" />
//           ) : (
//             <View />
//           )}
          
//         <Text style={{ color: "blue", fontWeight: "700", textAlign:'left' }}>Дэлгэрэнгүй</Text>
//       </View>
//       <View style={styles.row}>
//         <Text>{formattedDate}</Text>
//       </View>
      

    
//     </View>
//   </TouchableHighlight>
<TouchableHighlight
activeOpacity={0.9}
underlayColor={theme === "white" ? "#1c3587" : "rgba(0,0,0,0.5)"}
style={[{
  marginVertical: 4,
  borderRadius: 15,
  backgroundColor: "#fff",
  borderWidth: 0.3,
  width: "94%",
  marginHorizontal: "3%",
  borderColor: "#ddd",
  borderBottomWidth: 1,
  shadowColor: "#000",
  shadowOffset: { width: 1, height: 1 },
  shadowOpacity: 0.3,
  shadowRadius: 1,
  elevation: 1,
},theme === "white" ? {} : { backgroundColor: "rgba(0,0,0,0.7)" },]}
onPress={() => {
  navigation.navigate("AsapDetail", { item, setasapsaw, asapsaw });
}}
>
<View style={styles.container}>
  <View style={styles.content}>
    <View style={styles.row}>
      <View style={styles.column}>
      <Image
        style={styles.logo}
        resizeMode="contain"
        source={{
          uri: item?.MPICURL,
        }}
      />
     <Text style={[{ color: 'blue', marginBottom: 5 },theme === "white" ? {} : { color: "#FFF" },]}>{item?.REGISTER?.toUpperCase()}</Text>
     </View>
      <View style={{ marginLeft: 5, flex: 1 }}>
        <Text style={[styles.header, theme === "white" ? {} : { color: "#FFF" }]}>{datas?.item?.MTYPE}</Text>
        <Text style={[{ color: 'blue', marginBottom: 5 },theme === "white" ? {} : { color: "#FFF" }]}>{item?.MINFO}</Text>
        <Text style={[{ textAlign: "justify" },theme === "white" ? {} : { color: "#FFF" }]} numberOfLines={4}>{datas?.item?.M5400INFO}</Text>
      </View>
    </View>
    <View style={[styles.row, { marginTop: 10 }]}>
      {loading ? (
        <ActivityIndicator size="small" color="blue" />
      ) : asapsaw == true ? (
        <MaterialCommunityIcons name="check" size={24} color="green" />
      ) : (
        <View />
      )}
      <Text style={[{ color: "blue", fontWeight: "700", textAlign: 'left' },theme === "white" ? {} : { color: "#FFF" }]}>Дэлгэрэнгүй</Text>
    </View>
    <View style={styles.row}>
      <Text style={theme === "white" ? {} : { color: "#FFF" }}>{formattedDate}</Text>
    </View>
  </View>
</View>
</TouchableHighlight>
  );
};

export default codeList;
const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
        marginVertical: 4,
        
        padding: 8,
      },
      row: {
        //paddingTop: 10,
        flexDirection: "row",
        justifyContent: "space-between",
      },
      column: {
        //paddingTop: 10,
        flexDirection: "column",
        //justifyContent: "space-between",
      },
      header: {
        fontWeight: "700",
        fontSize: 15,
        paddingVertical: 4,
      },
      item: {
        padding: 10,
        fontSize: 18,
        height: 44,
        borderBottomWidth: 1,
      },
      noInternet: {
        flex: 1,
        justifyContent: "center",
      },
      logo: {
        width: 100,
    
        height: 100,
      },
      img: {
        marginRight: 10,
        //justifyContent:'flex-start'
        
      },
      content:{
  
          flex: 1,
          justifyContent: 'space-between',
       
        
      }
});
