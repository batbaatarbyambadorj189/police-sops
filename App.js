import 'react-native-gesture-handler'
import RootNavigation from "./src/navigation/navigation";
import * as React from 'react';
import { useEffect,useState } from "react";
import { View, Text, AppState } from 'react-native';
import {initializeApp} from 'firebase/app';
import { DataContextProvider } from "./src/store/DataContext";
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

const fConfig = {
  databaseURL: "https://mn-npa-sop-default-rtdb.firebaseio.com/",
  project_id: "com.polite.Police_Code",
  storageBucket: "mn-npa-sop.appspot.com",
};
axios.defaults.baseURL = "http://www.rule.police.gov.mn";
export default  App = () => {
  const [appState, setAppState] = useState(AppState.currentState);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  useEffect(() => {
    initializeApp(fConfig);
    // const handleAppStateChange = async (nextAppState) => {
    //   console.log('App State:', nextAppState);
    //   if (nextAppState === 'background') {
    //     // App is going into the background, save the current time
    //     await AsyncStorage.setItem('backgroundTime', Date.now().toString());
    //   } else if (nextAppState === 'active') {
    //     // App is coming back to the foreground, check if 15 minutes have passed
    //     const backgroundTime = await AsyncStorage.getItem('backgroundTime');
    //     const elapsedTime = Date.now() - parseInt(backgroundTime);
    //     const fifteenMinutes = 1 * 60 * 1000; // 15 minutes in milliseconds

    //     if (elapsedTime >= fifteenMinutes && isLoggedIn) {
    //       // If more than 15 minutes have passed and the user is logged in, log them out
    //       console.log('15 minutes have passed. Logging out...');
    //       // Perform logout logic here (e.g., clear token, navigate to login screen)
    //       setIsLoggedIn(false);
    //     }
    //   }
    // };
    // AppState.addEventListener('change', handleAppStateChange);

    // return () => {
    //   AppState.removeEventListener('change', handleAppStateChange);
    // };
  }, []);
  // const checkTokenExpiry = async () => {
  //   try {
  //     const tokenExpiry = await AsyncStorage.getItem('tokenExpiry');
  //     console.log("tokenexpire",tokenExpiry)
  //     if (tokenExpiry && Date.now() >= parseInt(tokenExpiry)) {
  //       // Token has expired, perform logout
  //       console.log('Token has expired. Logging out...');
  //       // Perform logout logic here (e.g., clear token, navigate to login screen)
  //       setIsLoggedIn(false);
  //     }
  //   } catch (error) {
  //     console.error('Error checking token expiry:', error);
  //   }
  // };
  return (
    
    <DataContextProvider>
      <RootNavigation />
    </DataContextProvider>
   
    // <NavigationContainer independent={true}>
    //   <RootDrawer/>
    // </NavigationContainer>
  );
}
